<?php 
include_once("webservice.php");
global $GLOBAL_CONFIG;
$sugar = new sugarapi();
$mainfunc = new mainfunctions();
ini_set("display_errors", "1");
ini_set("error_reporting", "1");
/*

Session:- 
[userinfo] => Array
        (
            [session] => ft31qcsd0q0gkf9t842jgqcj56
            [user_id] => 26a03e90-045b-ad5d-7c1b-57a46e1f9045
            [user_name] => ankitgarg310
            [user_language] => en_us
            [user_currency_id] => -99
            [user_is_admin] => 
            [user_default_team_id] => 
            [user_default_dateformat] => m-d-Y
            [user_default_timeformat] => h:i A
            [user_number_seperator] => ,
            [user_decimal_seperator] => .
            [mobile_max_list_entries] => 
            [mobile_max_subpanel_entries] => 
            [user_currency_name] => US Dollars
            [first_name] => Ankit
            [last_name] => garg
            [full_name] => Ankit garg
            [email1] => ankitgarg3200@gmail.com
            [pic] => 
        )


*/

$session_id = $sugar->loginanyomys();
$action=$_REQUEST["action"];
if($action=="getaddress")
{
	$fields= array("billing_address_city","billing_address_street","billing_address_state","billing_address_postalcode");
	$query1="accounts.id='90024215-c406-2d79-fb17-593a7edd3c92'";
	$link_name_to_fields_array1="";
	$module_name="Accounts"; 
	$order_by=""; 
	$max_results=50;
	$deleted=0;
	$contact_detail_list = $sugar->get_entry_list($session_id,$module_name,$query1,$order_by,0,$fields,$link_name_to_fields_array1,$max_results,0,false); 
	$contact_detail_arr=$contact_detail_list->entry_list;
	
	$contactarr[] = $contact_detail_arr[0]->name_value_list->billing_address_street->value;
	$contactarr[] = $contact_detail_arr[0]->name_value_list->billing_address_state->value;
	$contactarr[] = $contact_detail_arr[0]->name_value_list->billing_address_city->value;
	$contactarr[] = $contact_detail_arr[0]->name_value_list->billing_address_postalcode->value;
	
	echo json_encode($contactarr);
}
if($action=="save_contact_info")
{
	$module_name = "Leads";
	$merge= array();
	
	foreach($_POST as $key=>$value){
	 $merge_vars[] =  array('name' => $key, 'value' => $value);
	}
	
	$inserted_val = $sugar->set_entry($session_id, $module_name, $merge_vars);
	
	// echo "<pre>";
	// print_r($inserted_val);
	// echo "</pre>";
	// exit;	
	echo json_encode(array("msg"=>"success"));
}
if($action=='signup')
{	
	$usertype="Individual User";
	$gitusername="";
	if($_POST['type']==2)
	{ 
		$chkemail = $mainfunc->check_company_email($_POST['email1']);
		if($chkemail=='sucess'){ 
			$data = array( "msg" => "Please Use Business Email Id");
			echo json_encode($data);
			exit;
		}
	}
	
	$signup_user= $sugar->check_signup_user($_POST['email1']);
	if($signup_user == '0')
	{ 
		//Create User in Users Module
		$signup_user=$sugar->create_signup($_POST['first_name'], $_POST['last_name'], $_POST['password'], $_POST['email1']);
		$_SESSION["password"] = $_POST['password'];
		$_SESSION["username"] = $_POST['email1'];
		$data= $sugar->login($_SESSION["username"], $_SESSION["password"], 'no');

		if($_POST['type']==2){ $usertype="Company User"; }
		//update user info
		$merge[] =  array('name' => 'id', 'value' => $signup_user);
 		$merge[] =  array('name' => 'user_type_c', 'value' => $usertype);
		$valusers= $sugar->set_entry($session_id,'Users',$merge);
		//Insert Record in Leads Module
		$merge_vars= array();
		foreach($_POST as $key=>$value){
		 $merge_vars[] =  array('name' => $key, 'value' => $value);
		}
		$valeads= $sugar->set_entry($session_id,'Leads',$merge_vars);
		
		if($_POST['type']==2){
			$email_id="company_".$_POST['email1'];
			$company_user=$sugar->create_signup_company($_POST['company_name'], $_POST['password'],$email_id, $signup_user);
			$data= $sugar->login($email_id, $password, 'no');
 			$_SESSION["password"] = $_POST['password'];
			$_SESSION["username"] = $_POST['email1'];
			$data= $sugar->login($_SESSION["username"], $_SESSION["password"], 'no');
		}
		if($signup_user !="")
		{
			$data = array( "msg" => "sucess", "logoutdropbox"=>$mainfunc->create_logout_dropbox());
		}else{	
			$data = array( "msg" => "Something Went Wrong");	
		} 
	}else{
		$data = array( "msg" => "Email Id Already Taken");
	}	
	echo json_encode($data);
	
}

if($action=='login')
{
	global $GLOBAL_CONFIG; 
	$email= $_POST['username'];
	$password= $_POST['password'];	
	
		if(isset($_POST['checkbox']) &&  $_POST['checkbox']='Remember'){
			$remember='yes';
		}else{
			$remember='no';
		}
		$username= $email;
		$data= $sugar->login($username, $password, $remember);
		
		if($data=="sucess"){ 
 		   $data = array( "msg" => "sucess","username" => $email, "logoutdropbox"=>$mainfunc->create_logout_dropbox()); 
		 
		}else{
			$data = array( "msg" => $data);
			
			}
	echo json_encode($data);
}
//forget password

if($action=='forget')
{
	$user_exist= $sugar->check_signup_user($_POST['username']);
	if($user_exist == '0')
	{
		$data = array("msg"=> "Email not Available");
	}
	else
	{
		$cur_date = date("Y-m-d H:i:s");   
		$table_name="users_password_link";
		$id="";
		
		//$merge_vars=array("deleted"=>"0","username"=>$_POST['username'],"date_generated"=>$cur_date);
		$merge_vars[] =  array('name' => 'username', 'value' => $_POST['username']);
		$merge_vars[] =  array('name' => 'date_generated', 'value' => $cur_date);
		$merge_vars[] =  array('name' => 'deleted', 'value' => '0');
		
		$reset_password_result = $sugar->set_table_entry($session_id, $table_name, $merge_vars, $id);
		
		/* echo "<pre>";
		print_r($reset_password_result);
		echo "</pre>";
		exit; */
		
		$id=$reset_password_result;
		
		include_once($comdir->api_lib_dir."includes/PHPMailer/PHPMailerAutoload.php");
		$mail = new PHPMailer;
		$sender_name = 'Ideadunes Admin Team';
		$from_mail = 'chaitanc@ideadunes.com';
		$to_mail = $_POST['username'];
		$to_cc = '';
		$to_bcc = ''; 
		$subject = "Reset Your Travel Management Password"; 
		
		$body_html = "Hello, <br />You recently requested to be able to reset your account password.<br />Click on the link below to reset your password:<br /><a href='".$GLOBAL_CONFIG["site_url"]."reset_password.php?id=$id'>Reset Password</a>";
		
		$body='';
							

		//$mail->SMTPDebug = 4;                               // Enable verbose debug output

		$mail->isSMTP();                                      // Set mailer to use SMTP
		//$mail->Host = 'smtp.gmail.com';  // Specify main and backup SMTP servers
		$mail->Host = 'yukon.websitewelcome.com';  // Specify main and backup SMTP servers
		$mail->SMTPAuth = true;                               // Enable SMTP authentication
		//$mail->Username = 'ideadunes.test4@gmail.com';                 // SMTP username
		$mail->Username = 'chaitanc@ideadunes.com';                 // SMTP username
		$mail->Password = 'chaitan@123';                           // SMTP password
		$mail->SMTPSecure = 'tls';                            // Enable TLS encryption, `ssl` also accepted
		$mail->Port = 25;                                    // TCP port to connect to

		$mail->setFrom($from_mail, 'Ideadunes Webapp');
		$mail->addAddress($to_mail, 'User');     // Add a recipient
		//$mail->addAddress($sender_name);               // Name is optional
		$mail->addReplyTo($to_mail, 'User');
		//$mail->addCC($to_cc);
		//$mail->addBCC($to_bcc);

		//$mail->addAttachment('');         // Add attachments
		//$mail->addAttachment('');    // Optional name
		$mail->isHTML(true);                                  // Set email format to HTML

		$mail->Subject = $subject;
		$mail->Body    =  html_entity_decode($body_html);
		$mail->AltBody = $body;
		
		if(!$mail->send()) {
			$data = array("msg"=> "Message could not be sent.");
			
		} else {
		$data = array( "msg" => "An Email Has Been Send to Your Email Provided. Please Follow the Intstruction Given in the Mail.");	
		} 
		/* try
		{
			if(!$mail->send())
			{
				echo "message could not be sent. <p>";
				echo "mailer error: " . $mail->ErrorInfo;
				exit;
			}
			echo 'after mail send';
		}
		catch(exception $e) 
		{
		  echo 'caught exception: ',  $e->getmessage(), "\n";
		} */
	}
	echo json_encode($data);
}

if($action=='updatepassword')
{
	/* include('classes/dbclass.php');
	$dbObject = new DBConnect();
	$dbConLink = $dbObject->conn;
$session_id = $sugar->loginanyomys();
$id = $_POST['id'];
$userid= $_POST['userid'];
	$passwordnew= md5($_POST['password']);
	$update_password = "UPDATE `users` SET `user_hash`='$passwordnew' where `id`= $userid";
	   $row_data= $dbObject->Update($update_password);
	
	
		$data = array( "msg" => "Your Passowrd Update successfully");
	   $update_status = "UPDATE `forget_password` SET `status`='0' where `id`= $id";
	   $row_data= $dbObject->Update($update_status);
		echo json_encode($user); */
	
	
	
	$id = $_POST['id'];
	$username = $_POST['username'];
	$passwordnew= md5($_POST['password']);
	
	//get user id of 
	$session_id = $sugar->loginanyomys();
	
	$module_name= "Users";
	$fields= array("id");
	$link_name_to_fields_array1="";
	$order_by="";
	$query1="users.user_name='$username'";
	//$user_list = $sugar->get_entry_list($session_id,$module_name,$query1,$order_by,0,$fields,$link_name_to_fields_array1,'',10,false); 
	$user_list = $sugar->get_entry_list($session_id, $module_name,$query1,$order_by,0,$fields, $link_name_to_fields_array1,'1',0,false);
	
	$user_arr = $user_list->entry_list;
	$userid = $user_arr[0]->name_value_list->id->value;
	// Update User password 
	//echo "Check Userid : $userid";exit;
	$merge_vars = 
			array ( 
				array('name' => 'id', 'value' => $userid),
				array('name' => 'user_hash', 'value' => $passwordnew),
			) ;
	$sugar->set_entry($session_id, $module_name, $merge_vars);
		
	$table_name="users_password_link";
	$merge_vars1[] =  array('name' => 'deleted', 'value' => '1');
	$result = $sugar->set_table_entry($session_id, $table_name, $merge_vars1, $id);
	/* 
	$query = "id='".$_GET["id"]."'";
	$order_by="";
	$offset="";
	$select_fields="";
	$max_results="";
	$get_record = $sugar->get_table_entry($session_id, $table_name, $query, $order_by, $offset, $select_fields, $max_results);
	echo "<pre>";
	print_r($get_record);
	echo "</pre>";
	exit; */
	if($result==$id)
		$data = array( "msg" => "Your Password Update successfully!!!");
	else
		$data = array( "msg" => "Oopss!! Something went wrong!!");
	
	echo json_encode($data);
}

if($action=='savefeedback')
{
	$session_id= $_SESSION["userinfo"]["session"];
	$createby = $_SESSION["userinfo"]["user_id"];
	$parent_id = $_POST["parent_id"]; // This is for parent name is feedback module
	$description = $_POST["description"];
	$feedbackname = rtrim($mainfunc->get_approx_chars($description, 20));
	$module_name = $GLOBAL_MODULE_NAME["feedback"];
	$merge_vars = 
			array ( 
				array('name' => 'name', 'value' => $feedbackname),
				array('name' => 'parent_type', 'value' =>$GLOBAL_MODULE_NAME["website"]),
				array('name' => 'status_c', 'value' =>"Inactive"),
				array('name' => 'parent_id', 'value' =>$parent_id),
				array('name' => 'created_by', 'value' =>$createby),
				array('name' => 'description', 'value' => $description),
			);
			
			
	
	$val = $sugar->set_entry($session_id, $module_name, $merge_vars);
	
	// echo "<pre>";
	// print_r($val);
	// echo "</pre>";
	// exit;	
	
	$data = array( "msg" => "success");
	echo json_encode($data);
}

if($action=='savecomment')
{
	$session_id= $_SESSION["userinfo"]["session"];
	$createby = $_SESSION["userinfo"]["user_id"];
	$parent_id = $_POST["parent_id"]; // This is for parent name of blog
	$comment_description_c = $_POST["comment_description_c"];
	$module_name = $GLOBAL_MODULE_NAME["comment"];
	$name= substr($comment_description_c, 0, 20);

	$merge_vars = 
			array ( 
				array('name' => 'parent_type', 'value' =>$GLOBAL_MODULE_NAME["blog"]),
				array('name' => 'status_c', 'value' =>"Reject"),
				array('name' => 'parent_id', 'value' =>$parent_id),
				array('name' => 'created_by', 'value' =>$createby),
				array('name' => 'assigned_user_id', 'value' =>$createby),
				array('name' => 'comment_description_c', 'value' => $comment_description_c),
				array('name' => 'name', 'value' => $name)

			);
			
			
	
	$val = $sugar->set_entry($session_id, $module_name, $merge_vars);
	
	// echo "<pre>";
	// print_r($val);
	// echo "</pre>";
	// exit;	
	//echo $val->id;
	if(isset($val->id))
		$data = array( "msg" => "success", "blog_id"=>$parent_id);
	else
		$data = array( "msg" => "unsuccess");
		
	echo json_encode($data);
}

if($action=='updatecommentview')
{
	$session_id= $_SESSION["userinfo"]["session"];
	$blog_id = $_POST["blog_id"];
	
	//Here will display all active comment
	$fields= array();
	$tablename = strtolower($GLOBAL_MODULE_NAME["comment"]);
	$query1="{$tablename}_cstm.parent_id='$blog_id' AND {$tablename}_cstm.status_c='Accept'";
	echo "Query is : $query1<br>";
	//$query1="";
	$link_name_to_fields_array1="";
	$module_name=$GLOBAL_MODULE_NAME["comment"]; 
	$order_by="date_entered desc"; 
	$max_results=100;
	$deleted=0;
	$comment_list = $sugar->get_entry_list($session_id,$module_name,$query1,$order_by,0,$fields,$link_name_to_fields_array1,$max_results,0,false); 
	$comment_arr=$comment_list->entry_list;
	// echo '<pre>';
	// print_r($comment_arr);
	// echo '</pre>'; 
	// exit;
	$output = '<div class="col-md-12 col-sm-12 col-xs-12">
				<h3>'.count($comment_arr).' Comments</h3>
			</div><div class="col-md-9 col-sm-9 col-xs-12">';
			if(count($comment_arr)>0)
			foreach($comment_arr as $key => $comment){
				$output .='<div class="col-md-12 col-sm-12 col-xs-12">
						<div class="col-md-2 col-sm-2">
						  <div class="thumbnail"> <img class="bimgs" src="https://ssl.gstatic.com/accounts/ui/avatar_2x.png"> </div>
						  <!-- /thumbnail --> 
						</div>
						<!-- /col-sm-1 -->
						
						<div class="col-md-10 col-sm-10">
						  <div class="panel panel-default">
							<div class="panel-heading"> <strong>'.$comment->name_value_list->created_by_name->value.'</strong> <span class="text-muted">commented ';
							
							$date1=date_create($comment->name_value_list->date_entered->value);
							$date2=date_create(date("Y-m-d"));
							$diff=date_diff($date1,$date2);
							$daysdiff = $diff->format("%R%a");
							if($daysdiff==0)
								$output .= "Today";
							else if($daysdiff==1)
								$output .= "Yesterday";
							else
								$output .= "$daysdiff days ago";
							
							$output .='	</span> 
							</div><div class="panel-body">'.$comment->name_value_list->comment_description_c->value.'</div>
							<!-- /panel-body --> 
						  </div>
						  <!-- /panel panel-default --> 
						</div>
						<!-- /col-sm-5 -->
					</div>';
			 }
			$output .='</div>';
			echo $output;
}
if($action=="loadmoreblogs")
{
	$startindex = $_REQUEST["startindex"];
	//echo "Check start index $startindex";
	$module_id=$MODULE_IDS["website_id"]; //Website id
	$module_name=$GLOBAL_MODULE_NAME["website"];
	//$link_field_name="abc12_website_abc12_blog_1"; //The field on feedback module which is linked with website
	//$link_field_name=strtolower($GLOBAL_MODULE_NAME["website"])."_".strtolower($GLOBAL_MODULE_NAME["blog"])."_1";
	$link_field_name=strtolower("$GLOBAL_MODULE_NAME[website]_$GLOBAL_MODULE_NAME[blog]_1");
	$related_module_query="";
	$related_fields= array("id", "name", "blog_page_c", "date_entered", "blog_page_content_c");
	$related_module_link_name_to_fields_array=array();
	$deleted=0;
	$order_by="date_entered desc";  
	$limit=2;
	$offset=$startindex;
	$blog_list = $sugar->get_relationship_list($session_id,$module_name,$module_id,$link_field_name,$related_module_query,$related_fields,$related_module_link_name_to_fields_array,$deleted,$order_by,$offset,$limit); //fetching Sub panel value
	$blog_value_arr=$blog_list->entry_list;
	
	// echo '<pre>';
	// print_r($blog_value_arr);
	// echo '</pre>';
	
	foreach($blog_value_arr as $blog)
	{
		$blogimagepath = $mainfunc->image_paths($blog->name_value_list->blog_page_c->value, "assets/images/no_image_available.jpg", '1');
		echo '<div class="row">
		<div class="col-md-3">
		  <div> <a href="blog_details.php?id='.$blog->name_value_list->id->value.'"> <img src="'. end($blogimagepath).'" data-src="'.end($blogimagepath).'" alt="'.$blog->name_value_list->name->value.'" class="img-responsive bimgs unveil image" /> </a> </div>
		</div>
		<div class="col-md-9">
		  <h4><a href="blog_details.php?id='.$blog->name_value_list->id->value.'">'.$blog->name_value_list->name->value.'</a></h4>
		  <div class="clearfix">
			<p class="date-comments"> <a href="blog_details.php?id='.$blog->name_value_list->id->value.'"><i class="fa fa-calendar-o"></i>'.date("F d, Y", strtotime($blog->name_value_list->date_entered->value)).'</a> </p>
		  </div>
		  <p class="introd">'.
			$mainfunc->get_approx_chars($blog->name_value_list->blog_page_content_c->value, 200).'
		  </p>
		  <p class="read-more"><a class="btn btn-primary" href="blog_details.php?id='.$blog->name_value_list->id->value.'">Continue reading</a></p>
		  <p></p>
		</div>
	  </div>';
	}
}
?>
 