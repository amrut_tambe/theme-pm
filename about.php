<?php 
	include("includes/header.php");

	$activeinactive =$mainfunc->internal_page_status("About");
	//echo "Check the by default status (Which should be Inactive, but nothing shows)".$activeinactive;exit;
	if($activeinactive == "Inactive" || !isset($activeinactive) || trim($activeinactive)=="")
	{
		header("location:index.php");// 
		exit;
	}
	
	/* echo "<pre>";
	print_r($detail_list_arr);
	echo "</pre>";
	exit;	 */

?>
<section id="blogdetail" class="blogdetail-section">
	<div class="container">
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <h3>Continue Reading of About</h3>
            </div>
        </div>
        <div class="row">
            <div class="col-md-9 col-sm-12 col-xs-12">
				<?php
					$aboutimagepath = $mainfunc->image_paths($detail_list_arr[0]->name_value_list->about_page_banner_c->value, "assets/images/aaaa.jpg", '1');
					if($aboutimagepath !="")
					{
						foreach($aboutimagepath as $imagepath)
							echo "<img  src='".$imagepath."' class='aboutimg'' alt='About Pic' />";
					}
				?>
				<p><?php echo html_entity_decode($detail_list_arr[0]->name_value_list->about_us_c->value);?></p>
            </div>
        </div>
       
		
</section>
<?php include("includes/footer.php");?>
