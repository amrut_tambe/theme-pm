<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <title>Ideadunes</title>
    <meta content="width=device-width, initial-scale=1.0" name="viewport">
    <meta content="" name="keywords">
    <meta content="" name="description">
    
    <!-- Facebook Opengraph integration: https://developers.facebook.com/docs/sharing/opengraph -->
    <meta property="og:title" content="">
    <meta property="og:image" content="">
    <meta property="og:url" content="">
    <meta property="og:site_name" content="">
    <meta property="og:description" content="">
    
    <!-- Twitter Cards integration: https://dev.twitter.com/cards/  -->
    <meta name="twitter:card" content="summary">
    <meta name="twitter:site" content="">
    <meta name="twitter:title" content="">
    <meta name="twitter:description" content="">
    <meta name="twitter:image" content="">

    <!-- Fav and touch icons -->
    <link rel="shortcut icon" href="img/icons/favicon.png">
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="img/icons/114x114.png">
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="img/icons/72x72.png">
    <link rel="apple-touch-icon-precomposed" href="img/icons/default.png">
  
    <!-- Google Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,100,300,500,700,900" rel="stylesheet">
    
    <!-- Bootstrap CSS File -->
    <link href="lib/bootstrap/css/bootstrap.min.css" rel="stylesheet">
  
    <!-- Libraries CSS Files -->
    <link href="lib/font-awesome/css/font-awesome.min.css" rel="stylesheet">
    <link href="lib/owlcarousel/owl.carousel.min.css" rel="stylesheet">
    <link href="lib/owlcarousel/owl.theme.min.css" rel="stylesheet">
    <link href="lib/owlcarousel/owl.transitions.min.css" rel="stylesheet">
	 <link href="css/custom.css" rel="stylesheet">
    
    <!-- Main Stylesheet File -->
 <link href="css/style.css" rel="stylesheet">
 <link href="#" id="colour-scheme" rel="stylesheet">
 </head>
  
<body class="page-index has-hero">
   <div id="background-wrapper" class="buildings" data-stellar-background-ratio="0.1">
       <div id="navigation" class="wrapper">
			<div class="header-hidden collapse">
			  <div class="header-hidden-inner container">
				<div class="row">
				  <div class="col-md-3">
					<h3>About Us</h3>
					<p>Flexor is a super flexible responsive theme with a modest design touch.</p>
					<a href="about.html" class="btn btn-more"><i class="fa fa-plus"></i> Learn more</a>
				  </div>
				  <div class="col-md-3">
					<h3> Contact Us</h3>
					<address>
						<strong>Flexor Bootstrap Theme Inc</strong> 
						<abbr title="Address"><i class="fa fa-pushpin"></i></abbr>
						Sunshine House, Sunville. SUN12 8LU.
						<br>
						<abbr title="Phone"><i class="fa fa-phone"></i></abbr>
						019223 8092344
						<br>
						<abbr title="Email"><i class="fa fa-envelope-alt"></i></abbr>
						info@flexorinc.com 
                </address>
              </div>
              <div class="col-md-6">
                <h3>Theme Variations </h3> 
                 <div class="switcher">
					<div class="cols">
						Backgrounds:
						<br>
						<a href="#benches" class="background benches active" title="Benches">Benches</a> <a href="#boots" class="background boots " title="Boots">Boots</a> <a href="#buildings" class="background buildings " title="Buildings">Buildings</a> <a href="#city" class="background city " title="City">City</a> <a href="#metro" class="background metro " title="Metro">Metro</a> 
					</div>
					<div class="cols">
                    Colours:
                    <br>
                    <a href="#orange" class="colour orange active" title="Orange">Orange</a> <a href="#green" class="colour green " title="Green">Green</a> <a href="#blue" class="colour blue " title="Blue">Blue</a> <a href="#lavender" class="colour lavender " title="Lavender">Lavender</a> 
                  </div>
                </div>
                <p>
                  <small>Selection is not persistent.</small>
                </p>
              </div>
            </div>
          </div>
        </div>
      
        <div class="header">
          <div class="header-inner container">
            <div class="row">
			<div class="col-md-3">
              <a class="navbar-brand" href="#" title="">
                  <h1 class="hidden">
                    <img src="img/index.jpg" alt="">
                   
                  </h1>
                </a>
           </div>
			 <nav class="navbar navbar-default">
      
        <div class="navbar-header">
            <button type="button" data-target="#navbarCollapse" data-toggle="collapse" class="navbar-toggle">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
         </div>
		<div class="col-md-9">
		<div id="navbarCollapse" class="collapse navbar-collapse">
            <ul class="nav navbar-nav">
                <li class="active"><a href="#">Home</a></li>
                <li><a href="#">Profile</a></li>
                <li><a href="#">Messages</a></li>
				 <li><a href="#">contact</a></li>
                <li><a href="#">portfolio</a></li> 
				<li><a href="#">services</a></li>
            </ul>
            <ul class="nav navbar-nav navbar-right">
                <li><a href="#">Login</a></li>
			</ul>
        </div>
    </nav>
</div>
</div>
</div>
 </div>
  </div>
      <div class="hero" id="highlighted">
        <div class="inner">
          <!--Slideshow-->
          <div id="highlighted-slider" class="container">
            <div class="item-slider" data-toggle="owlcarousel" data-owlcarousel-settings='{"singleItem":true, "navigation":true, "transitionStyle":"fadeUp"}'>
              <!--Slideshow content-->
              <!--Slide 1-->
              <div class="item">
                <div class="row">
                  <div class="col-md-6 col-md-push-6 item-caption">
                    <h2 class="h1 text-weight-light">
                      Welcome to <span class="text-primary">Ideadunes</span>
                    </h2>
                    <h4>
                      Super flexible responsive theme with a modest design touch.
                    </h4>
                    <p>Perfect for your App, Web service, company or portfolio! Magna tincidunt sociis ac integer amet non. Rhoncus augue? Tempor porttitor sed, aliquet phasellus a, nisi nunc aliquet nec rhoncus enim porttitor ultrices lacus tristique?</p>
                    
                  </div>
                  <div class="col-md-6 col-md-pull-6 hidden-xs">
                    <img src="img/slides/slide1.png" alt="Slide 1" class="center-block img-responsive">
                  </div>
                </div>
              </div>
              <!--Slide 2-->
              <div class="item">
                <div class="row">
                  <div class="col-md-6 text-right-md item-caption">
                    <h2 class="h1 text-weight-light">
                      <span class="text-primary">welcome</span> to Ideadunes
                    </h2>
                    <h4>
                      High quality, responsive theme!
                    </h4>
                    <p>Perfect for your App, Web service, company or portfolio! Magna tincidunt sociis ac integer amet non. Rhoncus augue? Tempor porttitor sed, aliquet phasellus a, nisi nunc aliquet nec rhoncus enim porttitor ultrices lacus tristique?</p>
                  
                  </div>
                  <div class="col-md-6 hidden-xs">
                    <img src="img/slides/slide2.png" alt="Slide 2" class="center-block img-responsive">
                  </div>
                </div>
              </div>
            </div>
          </div>
    </div>
    
    <!-- ======== @Region: #content ======== -->
    <div id="content">
     <div class="container containercolor1">
	 <div class="col-md-12">
		<div class="row">
		<h3>key Offering</h3>
		<div class="content">
		Cygnet is a one-stop destination for all your complex business problems. For over 15 years, we are committed to delivering technology enabled solutions through highly efficient Delivery mechanism and flexible engagement models. We enable business transformation for enterprises and technology providers by delivering customized, reliable and efficient solutions. Cygnet offers: 
	 </div><br><br>
	 </div>
	 </div>
	
		<div class="col-md-12">
			<div class="row">
				<div class="col-md-3">
					<img src="img/showcase/project1.png">
				</div>
					<div class="col-md-3">
					<h4>Application Development</h4>
					<div class="content1">Delivering high-performance apps on web, mobile and cloud to meet business challenges, empower users and elevate ROI.</div>
				</div>	
				<div class="col-md-3">
					<img src="img/showcase/project2.png">
				</div>	
				<div class="col-md-3">
					<h4>Application Development</h4>
					<div class="content1">
					Delivering high-performance apps on web, mobile and cloud to meet business challenges, empower users and elevate ROI.
					</div>
				</div>
				
			</div>
		</div>
		<div class="col-md-12">
			<div class="row">
				<div class="col-md-3">
					<h4>Application Development</h4>
					<div class="content1">Delivering high-performance apps on web, mobile and cloud to meet business challenges, empower users and elevate ROI.</div>
				</div>	
				<div class="col-md-3">
					<img src="img/showcase/project3.png">
				</div>
				<div class="col-md-3">
					<h4>Application Development</h4>
					<div class="content1">Delivering high-performance apps on web, mobile and cloud to meet business challenges, empower users and elevate ROI.</div
				</div>
				<div class="col-md-3">
					<img src="img/showcase/project4.png">
				</div>	
				
				
			</div>
		</div>
	</div>			
</div>
<!-----------------------------------------sucess stories------------------------------------------------------------>	
	    
	
	<div class="container containercolor2">
    <div class="page-header">
       <h2 class="block-title color1">
           Success Stories</h2>
    </div>
    <div class="row">
    	<div class="col-md-4">
            <div class="panel with-nav-tabs panel-default">
                <div class="panel-heading">
                    <ul class="nav nav-tabs">
						<li class="active"><a href="#tab1default" data-toggle="tab">HIPAA Compliant Electronic Medical Record (EMR) System</a>
						<!--<img src="img\misc\about-us.png">-->
						</li>
						<li><a href="#tab2default" data-toggle="tab">BI Solution for Public Bus Transportation</a></li>
						<li><a href="#tab3default" data-toggle="tab">SCORM Compliant LMS for learning on-the-go</a></li>
					</ul>
                </div>
            </div>
        </div>
        <div class="col-md-4">
            <div class="panel-body">
                <div class="tab-content">
					<div class="tab-pane fade in active" id="tab1default">HIPAA Compliant Electronic Medical Record (EMR) System</div>
					<div class="tab-pane fade" id="tab2default">BI Solution for Public Bus Transportation</div>
					<div class="tab-pane fade" id="tab3default">SCORM Compliant LMS for learning on-the-go</div>
					<div class="tab-pane fade" id="tab4default">.Net Solution for streamlining inventory</div>
                </div>
            </div>
        </div>
		 <div class="col-md-4">
             <div class="panel-body">
                    <div class="tab-content">
                        <div class="tab-pane fade in active" id="tab1default">HIPAA Compliant Electronic Medical Record (EMR) System</div>
                        <div class="tab-pane fade" id="tab2default">BI Solution for Public Bus Transportation</div>
                        <div class="tab-pane fade" id="tab3default">SCORM Compliant LMS for learning on-the-go</div>
                        <div class="tab-pane fade" id="tab4default">.Net Solution for streamlining inventory</div>
                       
                    </div>
                </div>
        </div>
	</div>
</div>

	
<!-----------------------------------------sucess stories end------------------------------------------------------------>	

<!-----------------------------------------About start------------------------------------------------------------>	

	
<div class="services block block-bg-gradient block-border-bottom">
        <div class="container containercolor3">
          <h2 class="block-title">
            About Cygnet</h2>
			<p>Our motto ‘IT is About You’ is more than just a tag line – it is the very heart of Cygnet. We always ensure the continued success of our clients and employees by placing problem solving ahead of anything else and walking the extra mile when needed.</p>
			<div class="row">
				<div class="col-md-3 text-center">
					<span class="fa-stack fa-5x">
					
					<i class="fa fa-group fa-stack-1x fa-inverse"></i> </span>
					<h4 class="text-weight-strong">
						User Experience / Information Architecture
					</h4>
					<p>Rhoncus adipiscing, magna integer cursus augue eros lacus porttitor magna. Dictumst, odio! Elementum tortor sociis in eu dis dictumst pulvinar lorem nec aliquam a nascetur.</p>
					
				</div>
            <div class="col-md-3 text-center">
              <span class="fa-stack fa-5x">
             
              <i class="fa fa-pencil fa-stack-1x fa-inverse"></i> </span>
              <h4 class="text-weight-strong">
                User Interface Design / User Interface Theming
              </h4>
              <p>Rhoncus adipiscing, magna integer cursus augue eros lacus porttitor magna. Dictumst, odio! Elementum tortor sociis in eu dis dictumst pulvinar lorem nec aliquam a nascetur.</p>
             
            </div>
            <div class="col-md-3 text-center">
              <span class="fa-stack fa-5x">
            
              <i class="fa fa-cogs fa-stack-1x fa-inverse"></i> </span>
              <h4 class="text-weight-strong">
                Code &amp; Development / Implementation Support
              </h4>
              <p>Rhoncus adipiscing, magna integer cursus augue eros lacus porttitor magna. Dictumst, odio! Elementum tortor sociis in eu dis dictumst pulvinar lorem nec aliquam a nascetur.</p>
             
            </div>
			<div class="col-md-3 text-center">
					<span class="fa-stack fa-5x">
					<i class="fa fa-group fa-stack-1x fa-inverse"></i> </span>
					<h4 class="text-weight-strong">
						User Experience / Information Architecture
					</h4>
					<p>Rhoncus adipiscing, magna integer cursus augue eros lacus porttitor magna. Dictumst, odio! Elementum tortor sociis in eu dis dictumst pulvinar lorem nec aliquam a nascetur.</p>
				
				</div>
          </div>
        </div>
      </div>
	  
	  
<!-----------------------------------------ABOUT end------------------------------------------------------------>	
	  
<!-----------------------------------------ABOUT end------------------------------------------------------------>	
<div id="section-partners" class="partners-logo">
	<div class="container containercolor4">
		<div class="row">
			<div class="col-md-12">
				<div class="col-md-2">
					<img src="img/index1.png" alt="MicroSoft Partner">
				</div>
				<div class="col-md-2">
					<img src="img/index5.png" alt="MicroSoft Partner">
				</div>
				<div class="col-md-2">
					<img src="img/index3.png" alt="MicroSoft Partner">
				</div>
				<div class="col-md-2">
					<img src="img/index1.png" alt="MicroSoft Partner">
				</div>
				<div class="col-md-2">
					<img src="img/index5.png" alt="MicroSoft Partner">
				</div>
				<div class="col-md-2">
					<img src="img/index3.png" alt="MicroSoft Partner">
				</div>
			</div>
		</div>
	</div>
</div>

<!-----------------------------------------ABOUT end------------------------------------------------------------>		  
	  
	  
<!------------------------------------------- contact us start --------------------------------------------------->
<div class="container containercolor6">
    <h2 class="block-title">Connect with us</h2>
		<div class="row">
			<div class="col-md-12">
				<div class="col-md-6">
				<?php
				$flagarr=array('usa.jpg','south-africa.jpg','australia.jpg','india.jpg','india.jpg','new-zealand.jpg','uk.jpg');
				$contaryarr=array("USA","South","Australia","India (HQ)","India (Pune)","New Zealand", "UK");
				$contactarr=array(" +1-646-915-0021"," +27 (0) 21 3002981"," +61-280-147-206","+91-79-67124000","+91-9727761717","+64 9 534 8000"," +44-20-8099-1653"," +49 3222 1090 129");
				for($i=0;$i<7;$i++)
				{
				
				?>
					<div class="row">
						<div class="col-md-2">
							<img src="img/flags/<?php echo $flagarr[$i];?>" class="countryflag">
					
						</div>
						<div class="col-md-3">
							<?php echo $contaryarr[$i];
							?>
						</div>
						<div class="col-md-7">
							<?php echo $contactarr[$i];
							?>
						</div>
					</div>
					<?php
					}
					?>
					
				</div>
				<div class="col-md-6">
					<form class="form-horizontal" role="form" method="post" action="index.php">
						<div class="form-group">
							<label for="name" class="col-sm-3 control-label">Name</label>
								<div class="col-sm-9">
									<input type="text" class="form-control" id="name" name="name" placeholder="First & Last Name" value="">
								</div>
							</div>
							<div class="form-group">
								<label for="email" class="col-sm-3 control-label">Email</label>
									<div class="col-sm-9">
										<input type="email" class="form-control" id="email" name="email" placeholder="example@domain.com" value="">
									</div>
								</div>
								<div class="form-group">
									<label for="email" class="col-sm-3 control-label">contact No</label>
										<div class="col-sm-9">
											<input type="email" class="form-control" id="contactno" name="contactno" placeholder="Contact No" value="">
										</div>
									</div>
									<div class="form-group">
										<label for="message" class="col-sm-3 control-label">Message</label>
										<div class="col-sm-9">
											<textarea class="form-control" rows="4" name="message"placeholder="Write Here your Message"></textarea>
										</div>
									</div>
									<div class="form-group">
										<div class="col-sm-9 col-sm-offset-3">
											<input id="submit" name="submit" type="submit" value="Send Message" class="btn btn-primary">
											<input id="submit" name="submit" type="submit" value="Reset" class="btn btn-primary">
										</div>
									</div>
									<div class="form-group">
										<div class="col-sm-10 col-sm-offset-2">
											<! Will be used to display an alert to the user>
										</div>
									</div>
								</form>
							</div>
						</div>
					</div>
				</div>
			</div>
  
  <!-----------------------------------------------footer start------------------------------------------------------>
 <div class="container containercolor7">
	<div class="footer">
		<div class="row">
			<div class="col-md-12">
				<div class="col-md-4">
					<div class="privacy">
					<a title="Privacy Policy" href="/privacy-policy">Privacy Policy</a>|
					<a title=" Terms &amp; Conditions" href="/terms-conditions">Terms &amp; Conditions</a>
					</div>
				</div>
				<div class="col-md-4">
					<div class="copyright">
					Copyright © 2017 <a href="www.ideadunes.com">Ideadunes</a>
					
					</div>
				</div>
				<div class="col-md-4">
				<div class="social">
					<ul class="list-inline list-unstyled">
                            <li><a class="linkedin" title="linkedin" target="_blank" href="http://www.linkedin.com/company/136943"><i class="fa fa-linkedin" aria-hidden="true"></i></a> </li>
                            <li><a class="twitter" title="twitter" target="_blank" href="https://twitter.com/cygnetinfotech"><i class="fa fa-twitter" aria-hidden="true"></i></a> </li>
                            <li><a class="google" title="google plus" target="_blank" href="https://plus.google.com/+Cygnetinfotech-India/posts"><i class="fa fa-google-plus" aria-hidden="true"></i></a> </li>
                            <li><a class="facebook" title="facebook" target="_blank" href="https://www.facebook.com/IT.is.about.you/"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
                        </ul>
					</div>
			
				</div>
			</div>		
		</div>
	</div>
</div>
  
  
  
   <!-----------------------------------------------footer start------------------------------------------------------>
  
   
    
    <!-- Required JavaScript Libraries -->
    <script src="lib/jquery/jquery.min.js"></script>
    <script src="lib/bootstrap/js/bootstrap.min.js"></script>
    <script src="lib/owlcarousel/owl.carousel.min.js"></script>
    <script src="lib/stellar/stellar.min.js"></script>
    <script src="lib/waypoints/waypoints.min.js"></script>
    <script src="lib/counterup/counterup.min.js"></script>
    <script src="contactform/contactform.js"></script>
    
    <!-- Template Specisifc Custom Javascript File -->
    <script src="js/custom.js"></script>
    
    <!--Custom scripts demo background & colour switcher - OPTIONAL -->
    <script src="js/color-switcher.js"></script>
    
    <!--Contactform script -->
    <script src="contactform/contactform.js"></script>
    
  
  </body>
</html>