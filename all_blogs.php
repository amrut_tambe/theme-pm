<?php 
	include("includes/header.php");
	$activeinactive =$mainfunc->internal_page_status("All Blogs");
	//echo $activeinactive;exit;
	if($activeinactive == "Inactive" || !isset($activeinactive) || trim($activeinactive)=="")
	{
		header("location:index.php");// 
		exit;
	}
?>
<section id="allblogs" class="allblogs-section">
  <div class="container">
    <div class="post" id="allpostedblogs">
    	<div class="row">
			<div class="col-sm-12">
				<div class="section-title">
					<h2 class="all">All Blogs</h2>
				</div>
			</div>
		</div>
		<?php
			$module_id=$MODULE_IDS["website_id"]; //Website id
			$module_name=$GLOBAL_MODULE_NAME["website"];
			//$link_field_name="abc12_website_abc12_blog_1"; //The field on feedback module which is linked with website
			//$link_field_name=strtolower($GLOBAL_MODULE_NAME["website"])."_".strtolower($GLOBAL_MODULE_NAME["blog"])."_1";
			$link_field_name=strtolower("$GLOBAL_MODULE_NAME[website]_$GLOBAL_MODULE_NAME[blog]_1");
			$related_module_query="";
			$related_fields= array("id", "name", "blog_page_c", "date_entered", "blog_page_content_c");
			$related_module_link_name_to_fields_array=array();
			$deleted=0;
			$order_by="date_entered desc";  
			$limit=200;
			$offset=0;
			$blog_list = $sugar->get_relationship_list($session_id,$module_name,$module_id,$link_field_name,$related_module_query,$related_fields,$related_module_link_name_to_fields_array,$deleted,$order_by,$offset,$limit); //fetching Sub panel value
			$blog_value_arr=$blog_list->entry_list;
			
			// echo '<pre>';
			// print_r($blog_value_arr);
			// echo '</pre>';
			$counter = 1;
			foreach($blog_value_arr as $blog)
			{
				if($counter==3)
					break;
					$blogimagepath = $mainfunc->image_paths($blog->name_value_list->blog_page_c->value, "assets/images/no_image_available.jpg", '1');
				?>
			  <div class="row">
				<div class="col-md-3">
					<div> <a href="blog_details.php?id=<?php echo $blog->name_value_list->id->value; ?>"> <img src="<?php echo end($blogimagepath);?>" data-src="<?php echo end($blogimagepath);?>" alt="Blog Image" class="img-responsive bimgs unveil image" /> </a> </div>
				</div>
				<div class="col-md-9">
				  <h4 class="head"><a href="blog_details.php?id=<?php echo $blog->name_value_list->id->value; ?>"><?php echo $blog->name_value_list->name->value;?></a></h4>
				  <div class="clearfix">
					<p class="date-comments"> <a href="blog_details.php?id=<?php echo $blog->name_value_list->id->value; ?>"><i class="fa fa-calendar-o"></i> <?php echo date("F d, Y", strtotime($blog->name_value_list->date_entered->value));?></a> </p>
				  </div>
				  <p class="page">
				  <?php
					echo $mainfunc->get_approx_chars($blog->name_value_list->blog_page_content_c->value, 200);
				  ?>
				  </p>
				  <p class="read-more"><a class="btn btn-primary" href="blog_details.php?id=<?php echo $blog->name_value_list->id->value; ?>">Continue reading</a></p>
				  <p></p>
				</div>
			  </div>
			  <?php
			  $counter++;
			}
		?>
    </div>
		<div id="loader" style="text-align:center; width:100%;"><img src="assets/images/ajax-loader.gif" style="width:60px; height:60px;    margin-left:500px;" /></div>
			<div class="margin-top-20 margin-bottom-20"></div>
				<input type="hidden" id="max_blog_rows" value="<?php echo count($blog_value_arr);?>">		
				<input type="hidden" id="blog_row_no" value="4">		
			</div>
		</section>
		
<?php include("includes/footer.php");?>

<script type="text/javascript">
$(window).scroll(function ()
{
	if($(document).height() <= $(window).scrollTop() + $(window).height())
	{
		var maxblogs = parseInt($("#max_blog_rows").val());
		var blogrow_offset = parseInt($("#blog_row_no").val());
		//alert(blogrow_offset+" : "+maxblogs);
		if(blogrow_offset<maxblogs)
		{
			loadmore();
		}
	}
});

function loadmore()
{
	var offset = parseInt($("#blog_row_no").val());
	$('#loader').show();
	$.ajax({
	type: 'post',
	url: 'ajax.php',
	data: "action=loadmoreblogs&startindex="+offset,
	success: function (response) {
	 // alert(response);
		$('#loader').hide();
		$("#allpostedblogs").append(response);
		// We increase the value by 2 because we limit the results by 2
		$("#blog_row_no").val(offset+2);
  }
	});
}

</script>
