
 <div class="carousel fade-carousel slide" data-ride="carousel" data-interval="4000" id="home">
  <!-- Overlay -->
  <div class="overlay"></div>

  
  <!-- Wrapper for slides -->
  <div class="carousel-inner">
    <div class="item slides active">
      <div class="slide-1"></div>
      <div class="hero">
        <hgroup>
            <h1><?php echo $detail_list_arr[0]->name_value_list->name->value;?></h1>        
            <h5 class="homecon"><?php echo (html_entity_decode($detail_list_arr[0]->name_value_list->overview_c->value));?>
							</h5>
        </hgroup>
      
	
	<style type="text/css">
			
			<?php
				$homeimagepath = $mainfunc->image_paths($detail_list_arr[0]->name_value_list->homepagebanner_c->value, "assets/images/no_image_available.jpg", '1');
				
			?>
				.fade-carousel .slides .slide-1{
				  height: 100vh;
				  background-size: cover;
				  background-position: center center;
				  background-repeat: no-repeat;
				  background-image: url("<?php echo end($homeimagepath);?>");
				}
	 
		</style>
		
	
      </div>
    </div>
   