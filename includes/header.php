<?php 
	include_once("webservice.php");
	global $GLOBAL_CONFIG;
	$sugar = new sugarapi();
	$mainfunc = new mainfunctions();
	ini_set("display_errors", "1");
	ini_set("error_reporting", "1");
	$objectdbtype=new Datatype();
	$session_id = $sugar->loginanyomys();
	$fields= array();
	$tablename = strtolower($GLOBAL_MODULE_NAME["website"]);
	$query1="$tablename.id='".$MODULE_IDS["website_id"]."'";
	$link_name_to_fields_array1="";
	$module_name=$GLOBAL_MODULE_NAME["website"]; 
	$order_by=""; 
	$max_results=1;
	$deleted=0;
	$basic_detail_list = $sugar->get_entry_list($session_id,$module_name,$query1,$order_by,0,$fields,$link_name_to_fields_array1,$max_results,0,false); 
	$detail_list_arr = $basic_detail_list->entry_list;
	
	// echo "<pre>";
	// print_r($detail_list_arr);
	// echo "</pre>";
	//exit;
	
	
	$field_value = $sugar->get_module_fields($session_id,$module_name);	//Fetch all the labels for	
	//$field_value1 = $sugar->get_module_fields($session_id,"abc12_Blog");	//Fetch all the labels for
	/* echo "<pre>";
	print_r($field_value);
	echo "</pre>";
	exit;	 */
	// echo "<pre>";
	// print_r($detail_list_arr);
	// echo "</pre>";
	// exit;
	$fields= array();
	$query1="accounts.id='$MODULE_IDS[account_id]'";
	$link_name_to_fields_array1="";
	$module_name="Accounts"; 
	$order_by=""; 
	$max_results=50;
	$deleted=0;
	$contact_detail_list = $sugar->get_entry_list($session_id,$module_name,$query1,$order_by,0,$fields,$link_name_to_fields_array1,$max_results,0,false); 
	$contact_detail_arr=$contact_detail_list->entry_list;
	// echo '<pre>';
	// print_r($contact_detail_arr); 
	// echo '</pre>';
		$serverpage =(strpos($_SERVER["SCRIPT_NAME"], "index.php")===false)?"index.php":"";
?>
	
<!DOCTYPE html>
<html lang="en">
  <head>
   <!-- SEO -->
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="description" content="<?php echo $detail_list_arr[0]->name_value_list->seo_short_description_c>value;?>">
    <meta name="author" content="<?php echo $detail_list_arr[0]->name_value_list->select_theme_c>value;?>">
    <meta name="url" content="<?php echo $detail_list_arr[0]->name_value_list->site_url->value;?>">
    <meta name="copyright" content="<?php echo $detail_list_arr[0]->name_value_list->name->value;?>">
    <meta name="robots" content="index,follow">
    
    
    <title><?php echo $detail_list_arr[0]->name_value_list->name->value;?></title>
    
    <!-- Facebook Opengraph integration: https://developers.facebook.com/docs/sharing/opengraph -->
    <meta property="og:title" content="">
    <meta property="og:image" content="">
    <meta property="og:url" content="">
    <meta property="og:site_name" content="">
    <meta property="og:description" content="">
    
    <!-- Twitter Cards integration: https://dev.twitter.com/cards/  -->
    <meta name="twitter:card" content="summary">
    <meta name="twitter:site" content="">
    <meta name="twitter:title" content="">
    <meta name="twitter:description" content="">
    <meta name="twitter:image" content="">

    <!-- Fav and touch icons -->
	
<?php
	
	$iconpath = $mainfunc->image_paths($detail_list_arr[0]->name_value_list->favicon_c->value, "assets/images/favicon/favicon.ico", "0");
	if($iconpath!=""){
?>
<link rel="shortcut icon" href="<?php echo end($iconpath); ?>">
<?php
	}
?>
   
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="img/icons/114x114.png">
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="img/icons/72x72.png">
    <link rel="apple-touch-icon-precomposed" href="img/icons/default.png">
	<script src="assets/lib/jquery/jquery.min.js"></script>
    <!-- Google Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,100,300,500,700,900" rel="stylesheet">
    <!-- Bootstrap CSS File -->
	
	<link rel='stylesheet prefetch' href='http://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css'>
    <link href="assets/lib/bootstrap/css/bootstrap.min.css" rel="stylesheet">
  
    <!-- Libraries CSS Files -->
    <link href="assets/lib/owlcarousel/owl.carousel.min.css" rel="stylesheet">
    <link href="assets/lib/owlcarousel/owl.theme.min.css" rel="stylesheet">
    <link href="assets/lib/owlcarousel/owl.transitions.min.css" rel="stylesheet">
	 <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/owl-carousel/1.3.3/owl.carousel.min.css">
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/owl-carousel/1.3.3/owl.theme.min.css">
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/owl-carousel/1.3.3/owl.transitions.css">

	 
    <!-- Hook in header section -->
	<?php echo $detail_list_arr[0]->name_value_list->in_head_c->value;?>
	
    <!-- Main Stylesheet File --> 
	
 <link href="assets/css/style.css" rel="stylesheet">
 <link href="assets/css/custom.css" rel="stylesheet">
  
 </head>
 <!---------------------	All Popup modal for signin-signup, forgotpassword,   ------------------------------>
<?php
	include_once($comdir->api_lib_dir."includes/signin-signup-modal.php");
	include_once($comdir->api_lib_dir."includes/forgot-password-modal.php");
	include_once($comdir->api_lib_dir."includes/feedback-modal.php");
?>
<!--------------------------------------------------	Feedback Modal Popup  start       -------------------------->
<body data-spy="scroll" data-target=".navbar" data-offset="50">

<!-- Hook before container section --> 
<?php echo $detail_list_arr[0]->name_value_list->before_container_c->value;?> 
  <nav class="navbar navbar-default">
  <div class="container-fluid">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <!--<a class="navbar-brand" href="#">Brand</a>-->
    </div>

    <!-- Collect the nav links, forms, and other content for toggling -->
    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
      <ul class="nav navbar-nav">
		<?php
			$logourl = $detail_list_arr[0]->name_value_list->logo_url_c->value;
			if(!isset($logourl) || (isset($logourl) && strlen($logourl)<10) || strpos($logourl, 'http')===false)
				$logourl=$GLOBAL_CONFIG["site_url"];
		?>
	   <a href="<?php echo $logourl;?>">
		<?php
			$logoimagepath = $mainfunc->image_paths($detail_list_arr[0]->name_value_list->logo_c->value, "assets/images/default-company-logo.gif", '0');
			if($logoimagepath !="")
			{
				foreach($aboutimagepath as $imagepath)
					echo "<img  src='".$imagepath."' class='sitelogo' width='100%' height='100%' alt='Logo' />";
			}
	   ?>
	</a>
      </ul>
     
      <ul class="nav navbar-nav navbar-right">
        <?php
			$menuarray = $mainfunc->menu_array();
			foreach($menuarray as $menu_name)
			{
				echo '<li><a class="smoth-scroll" href="'.$serverpage.'#'.strtolower($menu_name).'" >'.$menu_name.'<div class="ripple-wrapper"></div></a>';
			}
		?>
		<li class="loginlink">
			<?php if(isset($_SESSION["userinfo"]["user_name"])) {
				echo $mainfunc->create_logout_dropbox();
			} else {?>
				<a data-toggle="modal" data-target="#myModal" href="javascript:void(0)">Signin/Signup</a><?php }?>
		</li>
				
      </ul>
	   <?php

			$queryarr=array();
			foreach($servicesid as $sid)
			$queryarr[] = "web_services.id='$sid'";
			//echo implode(' OR ',$queryarr);exit;
			$fields= array();
			$query1=implode(' OR ',$queryarr);
			$link_name_to_fields_array1="";
			$module_name=$GLOBAL_MODULE_NAME["services"]; 
			$order_by=""; 
			$max_results=50;
			$deleted=0;
			$all_page_content_list = $sugar->get_entry_list($session_id,$module_name,$query1,$order_by,0,$fields,$link_name_to_fields_array1,$max_results,0,false); 
			$all_page_content_arr=$all_page_content_list->entry_list;
			//echo substr(html_entity_decode($all_page_content_arr[0]->name_value_list->overview->value),0, 30);
			//$viewable_page_content_arr = json_encode($all_page_content_arr);
			foreach($all_page_content_arr as $val)
			{
				$viewable_page_content_arr[$val->name_value_list->name->value] = $val->name_value_list;
			}
			$field_value = $sugar->get_module_fields($session_id,$module_name);	
			// echo "<pre>";
			// print_r($viewable_page_content_arr);
			// echo "</pre>";
			// exit;
		?>	
    </div><!-- /.navbar-collapse -->
  </div><!-- /.container-fluid -->
</nav>

