<?php
	// echo "<pre>";
	// print_r($_SESSION);
	// echo "</pre>";
?>
<ul class="nav navbar-nav navbar-right">
	<li class="dropdown">
		<a href="#" class="dropdown-toggle" data-toggle="dropdown">
			<i class="fa fa-user" aria-hidden="true"></i>
			<strong><?php echo $_SESSION["userinfo"]["first_name"];?></strong>
			<i class="fa fa-chevron-down" aria-hidden="true"></i>
		</a>
		<ul class="dropdown-menu">
			<li>
				<div class="navbar-login">
					<div class="row">
						<div class="col-lg-4 profile-image-div">
								<?php
									$userimage = (isset($_SESSION["userinfo"]["pic"]) && trim($_SESSION["userinfo"]["pic"])!="")?$GLOBAL_CONFIG["pic"]."/".$_SESSION["userinfo"]["pic"]:"assets/images/default_profile.png";
								?>
								<img src="<?php echo $userimage; ?>" class="profile-image" />
						</div>
						<div class="col-lg-8">
							<p class="text-left"><strong><?php echo $_SESSION["userinfo"]["full_name"];?></strong></p>
							<p class="text-left small"><?php echo $_SESSION["userinfo"]["email1"];?></p>
							<p class="text-left">
								<a href="#" class="btn btn-primary btn-block btn-sm">View Profile</a>
							</p>
						</div>
					</div>
				</div>
			</li>
			<li class="divider"></li>
			<li>
				<div class="navbar-login navbar-login-session">
					<div class="row">
						<div class="col-lg-12">
							<p>
								<a href="logout.php" class="btn btn-block btn-danger">Logout</a>
							</p>
						</div>
					</div>
				</div>
			</li>
		</ul>
	</li>
</ul>
