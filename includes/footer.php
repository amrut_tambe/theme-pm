<?php
	$feedformtype="1";
	if($feedformtype=="1")
	{
?>
	<?php if(isset($_SESSION["userinfo"]["user_name"])) {?>
	<span class="feedback"><a href="#" class="btn btn-primary feedbackbtn" data-toggle="modal" data-target="#myFeedbackModal"><i class="fa fa-envelope" aria-hidden="true"></i> Feedback</a></span>
	<?php } else {?>
	<input type="hidden" name="hf_wl" id="hf_wl" value="0" />
	<span class="feedback withoutlogin"><a href="#" class="btn btn-primary feedbackbtn" onclick="feedbackbtn_form();" data-toggle="modal" data-target="#myModal"><i class="fa fa-envelope" aria-hidden="true"></i>Feedback</a></span>
	<?php }?>
<?php
	} else if($feedformtype=="2"){
?>
	<div id="feedback">
		<div  id="feedbackdiv"  style='display:none;' class="col-xs-12 col-md-11 col-lg-11 panel panel-default">
			<form  id="feedback-form1"  method="POST" class="form panel-body" role="form" data-parsley-validate="">
				<div class="form-group">
					<?php	
						$data=array();
						$data['type']='Rating';
						$data['name']='description';
						$data['required']='1';
						$data['id']='description';
						$data['placeholder']='Enter your feedback here';
						$data['validation_field_name']='Feedback'; //name is requird field the message show like this.
						$data['errordivid']='feederrordiv'; //for error div to display parsley errors
						$data['help']=''; //for help text showing
						$data['class']=''; //class name u want to add
						$value=''; //if want to define any value in the text box
						echo $objectdbtype->getdatatype($data,$value,$modulename); ?>
					<input type="hidden" name="parent_id" value="<?php echo $detail_list_arr[0]->name_value_list->id->value;?>" />
				</div>
                
                <?php if(isset($_SESSION["userinfo"]["user_name"])) {?>
						<span id="slidingfeedback">
                       <button type="submit" name="sendfeedback" id="sendfeedback" class="form-control btn btn-primary pull-right">Send</button>
                    </span>
                <?php } else {?>
						<input type="hidden" name="hf_wl_slide" id="hf_wl_slide" value="0" />
                		<span id="slidingfeedback">
                        <a href="#" class="btn btn-primary" data-toggle="modal" data-target="#myModal">Send</a>
                        </span>
					
                <?php }?>
                
			</form>
		</div>
		<div id="feedback-tab"><a href="JavaScript:void(0);" class="btn btn-success"><i class="fa fa-envelope" aria-hidden="true"></i> Feedback</a></div>
	</div>
<?php
	}
?>
<?php echo $detail_list_arr[0]->name_value_list->after_container_c->value;?>

<!-----------------------------------------------footer start------------------------------------------------------>
 <div class="container containercolor7">
	<div class="footer">
		<div class="row">
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
				<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
					<div class="footerquicklink">
						<a href="<?php echo $serverpage;?>"><a href="#top" style=>Home</a>|<a href="<?php echo $serverpage;?>"><a href="#about">About</a>
					</div>
					<div class="privacy">
					<a title="Privacy Policy" href="/privacy-policy">Privacy Policy</a>|
					<a title=" Terms &amp; Conditions" href="/terms-conditions">Terms &amp; Conditions</a>
					</div>
				</div>
				<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
					<div class="copyright">
						Copyright © 2020 <a href="http://www.ideadunes.com" target="_blank">Ideadunes</a>
					</div>
				</div>
				<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
				<div class="social">
				<?php
					$module_id=$MODULE_IDS['account_id'];
					$module_name="Accounts";
					$link_field_name=strtolower("accounts_$GLOBAL_MODULE_NAME[social]_1");
					$related_module_query="status_c='Active'";
					$related_fields= array("name", "network_profile_url_c", "logo_c");
					$related_module_link_name_to_fields_array=array();
					$deleted=0;
					$order_by="";  
					$limit=200;
					$offset=0;
					$social_list = $sugar->get_relationship_list($session_id,$module_name,$module_id,$link_field_name,$related_module_query,$related_fields,$related_module_link_name_to_fields_array,$deleted,$order_by,$offset,$limit); //fetching Sub panel value
					$social_value_arr=$social_list->entry_list;
					// echo '<pre>';
					// print_r($social_value_arr); 
					// echo '</pre>';
	
					?>
					<ul class="list-inline list-unstyled">
					 <?php
						foreach($social_value_arr as $social)
						{
							
							$socialpicpath = $mainfunc->image_paths($social->name_value_list->logo_c->value, "assets/images/nosocialogo.png", '0');
							
							/* echo "<pre>";
							print_r($socialpicpath);
							echo "</pre>"; */
							if($socialpicpath !="")
							{
								echo "<li><a href='".$social->name_value_list->network_profile_url_c->value."' target='_blank'><img  src='".end($socialpicpath)."' width='50px' height='50px' alt='".$social->name_value_list->name->value."' /></a></li>";
							}
							else 
							{
								if(strtolower(trim($social->name_value_list->name->value))=="facebook")
									$social_class = 'facebook';
								else if(strtolower(trim($social->name_value_list->name->value))=="google+")
									$social_class = 'google-plus';
								else if(strtolower(trim($social->name_value_list->name->value))=="linkedin")
									$social_class = 'linkedin';
								else if(strtolower(trim($social->name_value_list->name->value))=="twitter")
									$social_class = 'twitter';
								else if(strtolower(trim($social->name_value_list->name->value))=="instagram")
									$social_class = 'instagram';
								else if(strtolower(trim($social->name_value_list->name->value))=="dribbble")
									$social_class = 'dribbble';
								
								echo '<li><a href="'.$social->name_value_list->network_profile_url_c->value.'" target="_blank" class="'.$social_class.'"><i class="fa fa-'.$social_class.'"></i></a></li>';
							}
						}
						?> 
                        </ul>
					</div>
				</div>
			</div>		
		</div>
	</div>
</div>
  
  
  
   <!-----------------------------------------------footer end------------------------------------------------------>
   
     <!-- Required JavaScript Libraries -->
	   
	<script type="text/javascript" src="<?php echo $comdir->api_lib_dir;?>assets/js/parsley/parsley.min.js"></script>
	<script src="assets/lib/bootstrap/js/bootstrap.min.js"></script>
    <script src="assets/lib/owlcarousel/owl.carousel.min.js"></script>
    <script src="assets/lib/stellar/stellar.min.js"></script>
    <script src="assets/lib/waypoints/waypoints.min.js"></script>
    <script src="assets/lib/counterup/counterup.min.js"></script>
	<script type="text/javascript" src="<?php echo $comdir->api_lib_dir;?>assets/js/custom-js-functions.js"></script>
    <script src="assets/js/custom.js"></script>
    <script src="assets/js/color-switcher.js"></script>
	<script src="https://pagead2.googlesyndication.com/pub-config/r20160913/ca-pub-3311815518700050.js"></script>

	<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/owl-carousel/1.3.3/owl.carousel.min.js"></script>
    <script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-593a2db97f39be6c"></script>
 
<script type="text/javascript" src="http://arrow.scrolltotop.com/arrow28.js"></script>
<noscript>Not seeing a <a href="http://www.scrolltotop.com/">Scroll to Top Button</a>? Go to our FAQ page for more info.</noscript>

 
	<?php echo $detail_list_arr[0]->name_value_list->after_footer_c->value;?>
  </body>
</html>