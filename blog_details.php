<?php 
if(isset($_GET["id"]) && strlen($_GET["id"])==36)
	$blog_id=$_GET["id"];
else
{
	header("location:index.php");
	exit;
}
include("includes/header.php");?>

<?php
	
	$fields= array();
	$tablename = strtolower($GLOBAL_MODULE_NAME["blog"]);
	$query1="$tablename.id='$blog_id'";
	$link_name_to_fields_array1="";
	$module_name=$GLOBAL_MODULE_NAME["blog"]; 
	$order_by="date_entered desc"; 
	$max_results=1;
	$deleted=0;
	$basic_detail_list = $sugar->get_entry_list($session_id,$module_name,$query1,$order_by,0,$fields,$link_name_to_fields_array1,$max_results,0,false); 
	$detail_list_arr=$basic_detail_list->entry_list;
	// echo '<pre>';
	// print_r($detail_list_arr);
	// echo '</pre>'; 
	// exit;
	if(isset($detail_list_arr[0]->name_value_list->blog_page_c->value) && trim($detail_list_arr[0]->name_value_list->blog_page_c->value)!="")

	$imagepath=$GLOBAL_CONFIG["crm_image_url"].str_replace("^", "", $detail_list_arr[0]->name_value_list->blog_page_c->value);
	else
	$imagepath="images/blog/avatar_1.png";


	//Change the flag of the read blog from 0 to 1 and enter the read "datetime"
	if(isset($blog_id))
	{
		$merge= array();
		$merge_vars[] =  array('name' => 'id', 'value' => $blog_id);
		$merge_vars[] =  array('name' => 'status_c', 'value' => '1');
		$merge_vars[] =  array('name' => 'date_time_c', 'value' => date("Y-m-d H:i:s"));
		$update_read_flag = $sugar->set_entry($session_id, $module_name, $merge_vars);
	}
?>
					


<section id="blogdetail" class="blogdetail-section">
	<div class="container">
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <h3 class="heading"><?php echo html_entity_decode($detail_list_arr[0]->name_value_list->name->value);?></h3>
            </div>
        </div>
        <div class="row">
            <div class="col-md-9 col-sm-12 col-xs-12">
                <div class="col-md-12 col-sm-12 col-xs-12">
				<?php
					$blogimagepath = $mainfunc->image_paths($detail_list_arr[0]->name_value_list->blog_page_c->value, "assets/images/no_image_available.jpg", '1');
					echo "<img  src='".end($blogimagepath)."' class='blogimg' alt='".$detail_list_arr[0]->name_value_list->name->value."' />";
				?>
                </div>
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <p><?php echo html_entity_decode($detail_list_arr[0]->name_value_list->blog_page_content_c->value);?></p>
                </div>
            </div>
            <div class="col-md-3 col-sm-12 col-xs-12">
                <div class="blogboxrs col-md-12 col-sm-12 col-xs-12">	
                	<?php
                    	//$module_id='9b651a15-7099-7796-af56-593a4d67e817'; //Website id
                    	$module_id=$MODULE_IDS["website_id"]; //Website id
						$module_name=$GLOBAL_MODULE_NAME["website"];
						//$link_field_name="web_website_blog_blog_1"; //The field on feedback module which is linked with website
						$link_field_name=strtolower("$GLOBAL_MODULE_NAME[website]_$GLOBAL_MODULE_NAME[blog]_1");
						$related_fields= array("id", "name", "status_c", "date_time_c");
						$related_module_link_name_to_fields_array=array();
						$deleted=0;
						$order_by="date_time_c desc";  
						$limit=5;
						$offset=0;
					
					?>
                    <div class="blogrightsidebar">New Posted Blogs</div>
                    <div class="bloglist">
                        <ul>
                            <?php
                               	$related_module_query="";
								$order_by="date_entered desc";  
                                $blog_list = $sugar->get_relationship_list($session_id,$module_name,$module_id,$link_field_name,$related_module_query,$related_fields,$related_module_link_name_to_fields_array,$deleted,$order_by,$offset,$limit); //fetching Sub panel value
                                $blog_value_arr=$blog_list->entry_list;
                                
                                 /*echo '<pre>';
                                 print_r($blog_value_arr);
                                 echo '</pre>';*/
                                foreach($blog_value_arr as $blog)
                                {
                                ?>					
                                    <li><a href="blog_details.php?id=<?php echo $blog->name_value_list->id->value; ?>"><?php echo substr($blog->name_value_list->name->value,0,18)."...";?></a></li>
                                <?php
                                }
                            ?>
                        </ul>
                    </div>
                </div>
                <div class="blogboxrs col-md-12 col-sm-12 col-xs-12">
                    <div class="blogrightsidebar">Recently Viewed Blogs</div>
                    <div class="bloglist">
                        <ul>
                            <?php
                                 $related_module_query="status_c=1";
								 $order_by="date_time_c desc";  
                                 $blog_list = $sugar->get_relationship_list($session_id,$module_name,$module_id,$link_field_name,$related_module_query,$related_fields,$related_module_link_name_to_fields_array,$deleted,$order_by,$offset,$limit); //fetching Sub panel value
                                $blog_value_arr=$blog_list->entry_list;
                                
                                // echo '<pre>';
                                // print_r($blog_value_arr);
                                // echo '</pre>';
                                foreach($blog_value_arr as $blog)
                                {
                                ?>					
                                    <li><a href="blog_details.php?id=<?php echo $blog->name_value_list->id->value; ?>"><?php echo substr($blog->name_value_list->name->value,0,18)."...";?></a></li>
                                <?php
                                }
                            ?>
                            
                        </ul>
                    </div>
                </div>
            </div>
			
        </div>
        <div class="row">
            <div class="col-md-9 col-sm-12 col-xs-12">
                <form action="" method="post" id="commentform" name="commentform" class="comment-form" data-parsley-validate="">
				
				<div class="panel-group">
					<div class="panel panel-default">
						<div class="panel-heading">Leave a Comment</div>
						<div class="panel-body">
							<div id="commenterrordiv"></div>
							<div class="form-group">
								<?php 
									$data=array();
									$data['type']='Rating';
									$data['name']='comment_description_c';
									$data['required']='1';
									$data['id']='comment_description_c';
									$modulename=$GLOBAL_MODULE_NAME["blog"];
									$data['placeholder']='Write Comment Details Here';
									$data['validation_field_name']='Comment Details'; //name is requird field the message show like this.
									$data['errordivid']='commenterrordiv'; //for error div to display parsley errors
									$data['help']=''; //for help text showing
									$data['class']=''; //class name u want to add
									$value=''; //if want to define any value in the text box
									echo $objectdbtype->getdatatype($data,$value,$modulename);
								?>
							</div>
							<div class="text-center"> 
								<input type="hidden" name="parent_id" value="<?php echo $blog_id;?>" />	
								<?php if(isset($_SESSION["userinfo"]["user_name"])) {?>
								<span class="postcom"><input type="button" id="btnsavecomment" class="btn btn-primary" onclick="save_comment_info();" value="Post" /></span>
								<?php } else {?>
								<span class="postcom"><input type="button" class="btn btn-primary" data-toggle="modal" data-target="#myModal" value="Post" /></span>
					
							<?php }?>
							</div>
						</div>
					</div>
				</div>
                </form>
            </div>
        </div>
		 <div class="row">
			<div class="col-md-12 col-sm-12 col-xs-12">
				<?php
					//Here will display all active comment
					$fields= array();
					$tablename = strtolower($GLOBAL_MODULE_NAME["comment"]);
					$query1="{$tablename}_cstm.parent_id='$blog_id' AND {$tablename}_cstm.status_c='Accept'";
					//echo "Query is : $query1<br>";
					//$query1="";
					$link_name_to_fields_array1="";
					$module_name=$GLOBAL_MODULE_NAME["comment"]; 
					$order_by="date_entered desc"; 
					$max_results=100;
					$deleted=0;
					$comment_list = $sugar->get_entry_list($session_id,$module_name,$query1,$order_by,0,$fields,$link_name_to_fields_array1,$max_results,0,false); 
					$comment_arr=$comment_list->entry_list;
				?>
				<h3><?php echo count($comment_arr);?> Comments</h3>
			</div>
			<div class="col-md-9 col-sm-9 col-xs-12">
			
					<?php
						
						// echo '<pre>';
						// print_r($comment_arr);
						// echo '</pre>'; 
						//exit;
						if(count($comment_arr)>0)
						foreach($comment_arr as $key => $comment){
					?>
				
					<div class="col-md-12 col-sm-12 col-xs-12">
						<div class="col-md-2 col-sm-2">
						  <div class="thumbnail"> <img class="img-responsive user-photo" src="https://ssl.gstatic.com/accounts/ui/avatar_2x.png"> </div>
						  <!-- /thumbnail --> 
						</div>
						<!-- /col-sm-1 -->
						
						<div class="col-md-10 col-sm-10">
						  <div class="panel panel-default">
							<div class="panel-heading"> <strong><?php echo $comment->name_value_list->created_by_name->value; ?></strong> <span class="text-muted">commented 
									<?php 
									$date1=date_create($comment->name_value_list->date_entered->value);
									$date2=date_create(date("Y-m-d"));
									$diff=date_diff($date1,$date2);
									$daysdiff = $diff->format("%R%a");
									if($daysdiff==0)
										echo "Today";
									else if($daysdiff==1)
										echo "Yesterday";
									else
										echo "$daysdiff days ago";
									?>
								</span> 
							</div>
							<div class="panel-body"><?php echo $comment->name_value_list->comment_description_c->value; ?></div>
							<!-- /panel-body --> 
						  </div>
						  <!-- /panel panel-default --> 
						</div>
						<!-- /col-sm-5 -->
					</div>
			<?php } ?>
			</div>
		</div>
		
    </div>
</section>

<?php include("includes/footer.php");?>
