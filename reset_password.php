<?php
include('webservice.php');
$sugar = new sugarapi();
$session = $sugar->loginanyomys();
 
$table_name="users_password_link";
$query = "id='".$_GET["id"]."'";
$order_by="";
$offset="";
$select_fields="";
$max_results="";
$get_record = $sugar->get_table_entry($session, $table_name, $query, $order_by, $offset, $select_fields, $max_results);
	// echo "<pre>";
	// print_r($get_record);
	// echo "</pre>";
	// exit;
	//echo $resetpassid = $get_record[0]->id;

?>
<!doctype html>
<!--[if lt IE 7]>      
<html class="no-js lt-ie9 lt-ie8 lt-ie7" lang="">
   <![endif]-->
   <!--[if IE 7]>         
   <html class="no-js lt-ie9 lt-ie8" lang="">
      <![endif]-->
      <!--[if IE 8]>         
      <html class="no-js lt-ie9" lang="">
         <![endif]-->
         <!--[if gt IE 8]><!--> 
         <html class="no-js" lang="">
            <!--<![endif]-->
            <head>
               <meta charset="utf-8">
               <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
			   
               <title>Ideadunes Web Interface</title>
               <meta name="description" content="">
               <meta name="viewport" content="width=device-width, initial-scale=1">
               <!-- ============================================
                  ================= Stylesheets ===================
                  ============================================= -->
               <!-- vendor css files -->
               <link rel="stylesheet" href="<?php echo $comdir->api_lib_dir;?>assets/css/resetpass-css/bootstrap.min.css">
               <link rel="stylesheet" href="<?php echo $comdir->api_lib_dir;?>assets/css/resetpass-css/animate.css">
               <link rel="stylesheet" href="<?php echo $comdir->api_lib_dir;?>assets/css/font-awesome.min.css">
               <link rel="stylesheet" href="<?php echo $comdir->api_lib_dir;?>resetpass-css/animsition.min.css">
               <!-- project main css files -->
               <link rel="stylesheet" href="<?php echo $comdir->api_lib_dir;?>assets/css/resetpass-css/main.css">
               <!--/ stylesheets -->

            </head>
            <body id="minovate" class="appWrapper">
               <!--[if lt IE 8]>
               <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
               <![endif]-->
               <!-- ====================================================
                  ================= Application Content ===================
                  ===================================================== -->
               <div id="wrap" class="animsition">
					<div class="page page-core page-login">
						<div class="text-center">
							<h3 class="text-light text-white"><span class="text-lightred">Ideadunes</span> Webapp</h3>
						</div>
						<div class="container w-420 p-15 bg-white mt-40 text-center">
								<?php
							/* 	$strStart= $_GET['time'];
								$url_date = date('Y-m-d h:i:s', $strStart);
								$cur_date = date('Y-m-d h:i:s', time()); */
								
								$link_generate_date = $get_record[0]->date_generated;
								$username = $get_record[0]->username;
								$deleted = $get_record[0]->deleted;
								$resetpassid = $get_record[0]->id;
								$current_date = date("Y-m-d H:i:s");  
								
								$dateStart = new DateTime($link_generate_date); 
								$dateEnd   = new DateTime($current_date); 
								
								$dateDiff  = $dateStart->diff($dateEnd); 
								$str_time=  $dateDiff->format("%H:%I:%S"); 
								
								
								sscanf($str_time, "%d:%d:%d", $hours, $minutes, $seconds);
								$time_seconds = isset($seconds) ? $hours * 3600 + $minutes * 60 + $seconds : $hours * 60 + $minutes;
								
								/*  
								sscanf($str_time, "%d:%d:%d", $hours, $minutes, $seconds);
								$time_seconds = isset($seconds) ? $hours * 3600 + $minutes * 60 + $seconds : $hours * 60 + $minutes;
								
								$sql_query= "SELECT * FROM forget_password where `id` = '".$_GET['id']."'";								
								$row_data= $dbObject->FetchObject($sql_query);
								$status = $row_data->status; 
								*/
								 
								if(count($get_record)==0){
									?> 
									 <form name="form1" class="form-validation mt-20" >
										<p class="help-block text-left">Ooops!! Something went wrong, try again!!</p>
										<div class="bg-slategray lt wrap-reset mt-40 text-left">
											<p class="m-0">
												<a href="index.php" class="btn btn-lightred b-0 text-uppercase">Back</a>
											</p>
										</div>
									</form> 
									<?php
								}
								else if($deleted =='1' || $time_seconds >'7200' ){
									?> 
									 <form name="form1" class="form-validation mt-20" >
										<p class="help-block text-left">Link Expired</p>
										<div class="bg-slategray lt wrap-reset mt-40 text-left">
											<p class="m-0">
												<a href="index.php" class="btn btn-lightred b-0 text-uppercase">Back</a>
											</p>
										</div>
									</form> 
									<?php
								}
								else{
									?>
									 <h2 class="text-light text-greensea">Reset Password </h2>
										<div class="alert alert-success" role="alert" id="success_msg"></div>
										<form name="form" id="form1" class="form-validation mt-20" data-parsley-validate>
										    <input type="hidden" name="id" value="<?php echo $resetpassid;?>"/>
											<input type="hidden" name="username" value="<?php echo $username;?>"/>
 											<div class="form-group">
												<input type="password" id="password" name="password" placeholder="Password" class="form-control underline-input"  minlength="8" required >
												
											</div>

											<div class="form-group">
												<input type="password" id="passwordConfirm" name="passwordConfirm" placeholder="Confirm Password" class="form-control underline-input" data-parsley-equalto="#password" required>
											</div>

											<div class="form-group text-left mt-20">
											<input type="hidden" name="action" value="updatepassword" />
											<a href="javascript:void(0);" class="btn btn-greensea b-0 br-2 mr-5 text-uppercase pull-right" id="resetSubmit">Reset</a>
												
											</div>

										</form>
								<?php }
								?>
						</div>
					</div>
			 </div>
			 
               <!--/ Application Content -->
                       <!-- ============================================
        ============== Vendor JavaScripts ===============
        ============================================= -->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>

        <script src="<?php echo $comdir->api_lib_dir;?>assets/js/resetpass-js/bootstrap.min.js"></script>

        <script src="<?php echo $comdir->api_lib_dir;?>assets/js/resetpass-js/jRespond.min.js"></script>

        <script src="<?php echo $comdir->api_lib_dir;?>assets/js/resetpass-js/jquery.slimscroll.min.js"></script>

        <script src="<?php echo $comdir->api_lib_dir;?>assets/js/resetpass-js/jquery.animsition.min.js"></script>
		
		<script src="<?php echo $comdir->api_lib_dir;?>assets/js/resetpass-js/parsley.min.js"></script>
        <!--/ vendor javascripts -->




        <!-- ============================================
        ============== Custom JavaScripts ===============
        ============================================= -->
        <script src="assets/js/resetpass-js/main.js"></script>
        <!--/ custom javascripts -->






        <!-- ===============================================
        ============== Page Specific Scripts ===============
        ================================================ -->
        <script>
            $(window).load(function(){
			 $("#success_msg").hide();
			 $(document).ajaxStart(function() {
				  $(".loading").show();
				  $(".black_overlay").show();
				  
				});

				$(document).ajaxComplete(function() {
				  $(".loading").hide();
				  $(".black_overlay").hide();
				});
			
			$( "#resetSubmit" ).click(function() {
				if($( "form" ).parsley().validate()){
				  $.ajax({
					url: 'ajax.php',
					data: $( "form" ).serialize(),
					type: 'POST',
					dataType:'json',
					success: function(result) {  
						
							document.getElementById('success_msg').innerHTML= result.msg;
							$("#success_msg").show();
								var timer = setTimeout(function() {
									window.location='index.php'
								}, 5000);
					  }
				});
			}	
			});

            });
        </script>
<div class="loading"><img src="<?php echo $comdir->api_lib_dir;?>assets/images/loading.gif" alt="Loading" /></div>
	<div class="black_overlay"></div>		
            </body>
         </html>