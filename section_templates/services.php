
<!-----------------------------------------feature section------------------------------------------------------------>	
<div class="container containercolor2" id="services">
    <div class="page-header">
       <h2 class="block-title color1">
         Latest Features</h2>
    </div>
    <div class="row text-center">
		<?php
		$module_id='a5ce212a-4470-b210-0d83-593aaf751f19';
		$module_name="AOS_Products";
		$link_field_name="aos_products_abc12_product_feature_1";
		$related_module_query="";
		$related_fields= array("id", "name");
		$related_module_link_name_to_fields_array=array();
		$deleted=0;
		$order_by="";  
		$limit=200;
		$offset=0;
		$product_list = $sugar->get_relationship_list($session_id,$module_name,$module_id,$link_field_name,$related_module_query,$related_fields,$related_module_link_name_to_fields_array,$deleted,$order_by,$offset,$limit); //fetching Sub panel value
		$product_value_arr=$product_list->entry_list;
		
		/* echo '<pre>';
		print_r($product_value_arr);
		echo '</pre>'; */
		
		$prodid_arr  = array();
		foreach($product_value_arr as $product)
		$prodid_arr[] = "abc12_product_feature.id='".$product->name_value_list->id->value."'";
		//echo '<pre>';
		//print_r($prodid_arr);

		$prodquerycondition = implode(" OR ", $prodid_arr);
		// echo '<pre>';
		// print_r($prodquerycondition);
		// exit;
		$fields= array("id", "name", "description","image_c");
		$query1=$prodquerycondition;
		$link_name_to_fields_array1="";
		$module_name="abc12_Product_Feature"; 
		$order_by=""; 
		$max_results=50;
		$deleted=0;
		$product_detail_list = $sugar->get_entry_list($session_id,$module_name,$query1,$order_by,0,$fields,$link_name_to_fields_array1,$max_results,0,false); 
		$product_detail_list_arr=$product_detail_list->entry_list;
		// echo '<pre>';
		// print_r($product_detail_list_arr);
		// echo '</pre>'; 
		//exit;
		foreach($product_detail_list_arr as $product_detail)
		{
			// echo '<pre>';
			// print_r($product_detail);
			// echo'<pre>';
		
	?>
				
		<div class="col-md-3 col-sm-6 hero-feature">
			<div class="thumbnail">
				<?php
					$blogimagepath = $mainfunc->image_paths($product_detail->name_value_list->image_c->value, "assets/images/no_image_available.jpg", '1');
					echo "<img  src='".end($blogimagepath)."' class='service' width='100%' height='100%' alt='".$product_detail->name_value_list->name->value."' />";
				?>
					<div class="caption">
						<h4><?php echo html_entity_decode($product_detail->name_value_list->name->value);?></h4>
						<p class="content1">
						<?php echo substr($product_detail->name_value_list->description->value,0,35)." ...";?></p>
						<p class="more">
							<a href="feature_details.php?id=<?php echo $product_detail->name_value_list->id->value;?>" target="_blank" class="btn btn-default">More Info</a>
						</p>
					</div>
				</div>
			</div>
			<?php 
			}
			?>
			
		</div>
      </div>
<!-----------------------------------------------feature end------------------------------------------------------------>	  


