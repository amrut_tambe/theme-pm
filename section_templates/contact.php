<!------------------------------------------- contact us start -------------------------------------------------------->

 
 <div class="container containercolor2" id="contact">
    <div class="page-header">
       <h2 class="block-title color1">
         Contact Us</h2>
    </div>
    <div class="row">
	<?php
		$fields= array();
		$query1="accounts.id='$MODULE_IDS[account_id]'";
		$link_name_to_fields_array1="";
		$module_name="Accounts"; 
		$order_by=""; 
		$max_results=50;
		$deleted=0;
		$contact_detail_list = $sugar->get_entry_list($session_id,$module_name,$query1,$order_by,0,$fields,$link_name_to_fields_array1,$max_results,0,false); 
		$contact_detail_arr=$contact_detail_list->entry_list;
		// echo '<pre>';
		// print_r($contact_detail_arr); 
		// echo '</pre>';
	?>
        <div class="col-md-6">
            <div class="well">
                <h3 style="line-height:20%;"><i class="fa fa-home fa-1x" style="line-height:6%;color:#339966;"></i> Address</h3>               
                <a href="google_map.html" target="_blank"><p style="margin-top:6%;margin-left:0px;"><?php echo $contact_detail_arr[0]->name_value_list->billing_address_street->value;?><br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<?php echo $contact_detail_arr[0]->name_value_list->billing_address_city->value;?>&nbsp;<?php echo $contact_detail_arr[0]->name_value_list->billing_address_state->value;?>&nbsp;&nbsp;<?php echo $contact_detail_arr[0]->name_value_list->billing_address_postalcode->value;?>&nbsp;<?php echo $contact_detail_arr[0]->name_value_list->billing_address_country->value;?></p></a>
                <br/>
                <br/>
                <h3 style="line-height:20%;"><i class="fa fa-envelope fa-1x" style="line-height:6%;color:#339966;"></i> E-Mail </h3>
                <p style="margin-top:6%;line-height:35%;margin-left:0px;"><a href="mailto:<?php echo $contact_detail_arr[0]->name_value_list->email1->value;?>"><?php echo $contact_detail_arr[0]->name_value_list->email1->value;?></a></p>
                <br />
                <br />

                <h3 style="line-height:20%;"><i class="fa fa-yelp fa-1x" style="line-height:6%;color:#339966"></i> Contact No</h3>
                <p style="margin-top:6%;line-height:35%"><a href="tel:<?php echo $contact_detail_arr[0]->name_value_list->phone_office->value;?>"><?php echo $contact_detail_arr[0]->name_value_list->phone_office->value;?></a></p>
            </div>
        </div>
        <div class="col-md-6">
          <div class="row">
			<form name="contactform" id="contactform" method="post">
					<div class="row">
					<div id="contacterrordiv"></div>
                         <div class="col-md-6">
                            <div class="form-group">
							<?php 
								$data=array();
								$data['type']='text';
								$data['name']='first_name';
								$data['required']='1';
								$data['id']='first_name';
								$modulename='Leads';
								$data['placeholder']='Write First Name Here';
								$data['validation_field_name']='First Name'; //name is requird field the message show like this.
								$data['errordivid']='contacterrordiv'; //for error div to display parsley errors
								$data['help']=''; //for help text showing
								$data['class']=''; //class name u want to add
								$value=''; //if want to define any value in the text box
								echo $objectdbtype->getdatatype($data,$value,$modulename);
							?>
                           </div>
                        </div>
						<div class="col-md-6">
							<div class="form-group">
							<?php 
								$data=array();
								$data['type']='text';
								$data['name']='last_name';
								$data['required']='';
								$data['id']='last_name';
								$modulename='Leads';
								$data['placeholder']='Write Last Name Here';
								$data['validation_field_name']='Last Name'; //name is requird field the message show like this.
								$data['errordivid']='contacterrordiv'; //for error div to display parsley errors
								$data['help']=''; //for help text showing
								$data['class']=''; //class name u want to add
								$value=''; //if want to define any value in the text box
								echo $objectdbtype->getdatatype($data,$value,$modulename);
							?>
                        </div>
                    </div>
                  </div>
                   <div class="row">
                     <div class="col-md-6">
                        <div class="form-group">
							<?php 
								$data=array();
								$data['type']='email';
								$data['name']='email1';
								$data['required']='1';
								$data['id']='email1';
								$modulename='Leads';
								$data['placeholder']='Write Email ID Here';
								$data['validation_field_name']='Email ID'; //name is requird field the message show like this.
								$data['errordivid']='contacterrordiv'; //for error div to display parsley errors
								$data['help']=''; //for help text showing
								$data['class']=''; //class name u want to add
								$value=''; //if want to define any value in the text box
								echo $objectdbtype->getdatatype($data,$value,$modulename);
							?>
                        </div>
                    </div>
                     <div class="col-md-6">
                         <div class="form-group">
						 <?php 
								$data=array();
								$data['type']='phone';
								$data['name']='phone_mobile';
								$data['required']='1';
								$data['id']='phone_mobile1';
								$modulename='Leads';
								$data['placeholder']='Your Mobile number here';
								$data['validation_field_name']='Mobile number'; //name is requird field the message show like this.
								$data['errordivid']='contacterrordiv'; //for error div to display parsley errors
								$data['help']=''; //for help text showing
								$data['class']=''; //class name u want to add
								$value=''; //if want to define any value in the text box
								echo $objectdbtype->getdatatype($data,$value,$modulename);
							?>
                        </div>
                     </div>
                 </div>
				   <div class="row">
                     <div class="col-md-6">
                        <div class="form-group">
							<?php 
								$data=array();
								$data['type']='text';
								$data['name']='address_c';
								$data['required']='1';
								$data['id']='address_c';
								$modulename='Users';
								$data['placeholder']='Your Contact Address here';
								$data['validation_field_name']='Contact Address'; //name is requird field the message show like this.
								$data['errordivid']='contacterrordiv'; //for error div to display parsley errors
								$data['help']=''; //for help text showing
								$data['class']=''; //class name u want to add
								$value=''; //if want to define any value in the text box
								echo $objectdbtype->getdatatype($data,$value,$modulename);
							?>
                        </div>
                    </div>
                     <div class="col-md-6">
                         <div class="form-group">
						<?php 
								$data=array();
								$data['type']='text';
								$data['name']='website';
								$data['required']='';
								$data['id']='website';
								$modulename='Users';
								$data['placeholder']='Your Website';
								//$data['validation_field_name']='Website'; //name is requird field the message show like this.
								$data['errordivid']='contacterrordiv'; //for error div to display parsley errors
								$data['help']=''; //for help text showing
								$data['class']=''; //class name u want to add
								$value=''; //if want to define any value in the text box
								echo $objectdbtype->getdatatype($data,$value,$modulename);
							?>
                        </div>
                     </div>
                 </div>
                  <div class="row">
                     <div class="col-md-12">
                        <div class="form-group">
						<?php 
								$data=array();
								$data['type']='text';
								$data['name']='description';
								$data['required']='1';
								$data['id']='description';
								$modulename='Leads';
								$data['placeholder']='Write your description Here';
								$data['validation_field_name']='Description'; //name is requird field the message show like this.
								$data['errordivid']='contacterrordiv'; //for error div to display parsley errors
								$data['help']=''; //for help text showing
								$data['class']=''; //class name u want to add
								$value=''; //if want to define any value in the text box
								echo $objectdbtype->getdatatype($data,$value,$modulename);
							?>
                        </div>
                     </div>
                     <div class="col-md-12">
						<input type="button" class="btn btn-primary" onclick="save_contact_info();" value="Submit" id="btnsavecontact" style="text-align: center;" />
						
                    </div>
                </div>
              </div>
			</form>
		</div> 
	</div>
</div>
	</div>
	
 <div class="container containercolor6" id="contact">
    <h2 class="block-title">Connect with us</h2>
		<div class="row">
			<div class="col-md-12">
			  <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d96690.80542089987!2d29.864461132544537!3d40.77109282810726!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x14cb4f66644bfb9d%3A0x82690ee7586b7eb9!2zxLB6bWl0LCBLb2NhZWxp!5e0!3m2!1str!2str!4v1480782606579" width="100%" height="430" frameborder="0" style="border:0" allowfullscreen></iframe>
		</div>
	</div>
</div>