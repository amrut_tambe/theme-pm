
<!-----------------------------------------feature section------------------------------------------------------------>	
<div class="container containercolor2" id="services">
    <div class="page-header">
       <h2 class="block-title color1">
         Latest Features</h2>
    </div>
    <div class="row text-center">
		<?php
		$module_id=$MODULE_IDS["product_id"]; //Website id
		$module_name=$GLOBAL_MODULE_NAME["products"];
		$link_field_name=strtolower("$GLOBAL_MODULE_NAME[products]_$GLOBAL_MODULE_NAME[features]_1");
		
		$related_module_query="";
		$related_fields= array("id", "name", "description" ,"image_c");
		$related_module_link_name_to_fields_array=array();
		$deleted=0;
		$order_by="";  
		$limit=200;
		$offset=0;
		$prodfeature_list = $sugar->get_relationship_list($session_id,$module_name,$module_id,$link_field_name,$related_module_query,$related_fields,$related_module_link_name_to_fields_array,$deleted,$order_by,$offset,$limit); //fetching Sub panel value
		$prodfeature_value_arr=$prodfeature_list->entry_list;
		
		// echo '<pre>';
		// print_r($prodfeature_value_arr);
		// echo '</pre>'; 
		// exit;
		
		foreach($prodfeature_value_arr as $feature_detail)
		{
			// echo '<pre>';
			// print_r($feature_detail);
			// echo'<pre>';
			
			if(isset($feature_detail->name_value_list->image_c->value) && trim($feature_detail->name_value_list->image_c->value)!="")

			$imagepath=$GLOBAL_CONFIG["crm_image_url"].str_replace("^", "", $feature_detail->name_value_list->image_c->value);
			else
			$imagepath="images/blog/avatar_1.png";
	?>
				
		<div class="col-md-3 col-sm-6 hero-feature">
			<div class="thumbnail">
				<?php
					$featureimagepath = $mainfunc->image_paths($feature_detail->name_value_list->image_c->value, "assets/images/no_image_available.jpg", '1');
					echo "<img  src='".end($featureimagepath)."' class='service' alt='".$feature_detail->name_value_list->name->value."' />";
				?>
					<div class="caption">
						<h4><?php echo html_entity_decode($feature_detail->name_value_list->name->value);?></h4>
						<p class="content1">
						<?php echo substr($feature_detail->name_value_list->description->value,0,30)." ...";?></p>
						<p class="more">
							<a href="feature_details.php?id=<?php echo $feature_detail->name_value_list->id->value;?>" target="_blank" class="btn btn-default">More Info</a>
						</p>
					</div>
				</div>
			</div>
			<?php 
			}
			?>
			 
		</div>
      </div>
<!-----------------------------------------------feature end------------------------------------------------------------>	  


