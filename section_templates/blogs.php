<!-----------------------------------------Our blog start------------------------------------------------>	
	    
	
	<div class="container containercolor2" id="blogs">
    <div class="page-header">
       <h2 class="block-title color1 text-center">
         Our Blog</h2>
    </div>
    <div class="row">
	<?php
		$module_id=$MODULE_IDS["website_id"]; //Website id
		$module_name=$GLOBAL_MODULE_NAME["website"];
		//$link_field_name="abc12_website_abc12_blog_1"; //The field on feedback module which is linked with website
		$link_field_name=strtolower("$GLOBAL_MODULE_NAME[website]_$GLOBAL_MODULE_NAME[blog]_1");
		$related_module_query="";
		$related_fields= array("id","name", "blog_page_c", "blog_page_content_c","date_entered","created_by_name" );
		$related_module_link_name_to_fields_array=array();
		$deleted=0;
		$order_by="date_created desc";  
		$limit=10;
		$offset=0;
		$blog_list = $sugar->get_relationship_list($session_id,$module_name,$module_id,$link_field_name,$related_module_query,$related_fields,$related_module_link_name_to_fields_array,$deleted,$order_by,$offset,$limit); //fetching Sub panel value
		$blog_value_arr=$blog_list->entry_list;
		$total_blogs = count($blog_value_arr);
		 // echo '<pre>';
		// print_r($blog_value_arr);
		// echo '</pre>';
		// exit;
		$blogarr=array();
		foreach($blog_value_arr as $blog)
		$blogarr[]=$blog;
		
	?>
		<div class="col-md-5 col-lg-5 col-sm-6 col-xs-12">
			<div class="featured-article">
				<a href="#">
				<?php
					$blogimagepath = $mainfunc->image_paths($blogarr[0]->name_value_list->blog_page_c->value, "assets/images/no_image_available.jpg", '1');
					echo "<img  src='".end($blogimagepath)."' class='blogimg1 text-center' width='100%' height='100%' alt='".$blog->name_value_list->name->value."' />";
				?>
				</a>
				<div class="block-title">
					<h2><?php echo substr($blogarr[0]->name_value_list->name->value,0,25)." ...";?></h2>
					<p class="by-author"><small><?php echo $blogarr[0]->name_value_list->created_by_name->value;?></small></p>
					<p class="by-author"><?php echo $blogarr[0]->name_value_list->date_entered->value;?></p>
						<a href="blog_details.php?id=<?php echo $blog->name_value_list->id->value;?>" target="_blank"class="hs_read_more hs_btn1">read more</a>
				</div>
			</div>
			<!-- /.featured-article -->
		</div>
		<div class="col-md-7 col-lg-7 col-sm-12 col-xs-12">
		
			<ul class="media-list main-list">
			<?php
				$blogcounter=1;
				for($i = 1; $i<count($blogarr); $i++)
				{
					if($blogcounter>=3)
							break;
				
			?>
			  <li class="media">
			    <a class="pull-left" href="#">
			     <?php
					$blogimagepath = $mainfunc->image_paths($blogarr[$i]->name_value_list->blog_page_c->value, "assets/images/no_image_available.jpg", '1');
					echo "<img  src='".end($blogimagepath)."' class='blogimgs' width='100%' height='100%' alt='".$blog->name_value_list->name->value."' />";
				 ?>
			    </a>
			    <div class="media-body">
			      <h4 class="media-heading"><?php echo substr($blogarr[$i]->name_value_list->name->value,0,25)." ...";?></h4>
				  
			      <p class="by-author"><?php echo $blogarr[$i]->name_value_list->created_by_name->value;?></p>
				  <p class="blogcont">
				  <?php echo $blogarr[$i]->name_value_list->date_entered->value;?></p>
				  
				  <p class="blogcont">
				  <?php
						$pagecontent = substr(strip_tags(html_entity_decode($blogarr[$i]->name_value_list->blog_page_content_c->value)),0,150)."...";
						echo $pagecontent;?>
					</p>
						<a href="blog_details.php?id=<?php echo $blogarr[$i]->name_value_list->id->value;?>" target="_blank"class="hs_read_more hs_btn1">read more</a>
			    </div>
			  </li>
			  <?php
				$blogcounter++;
				}
			  ?>
			</ul>
		</div>
	</div>
	<div class="blankdivider30"></div>
	<?php
		$activeinactive =$mainfunc->internal_page_status("All Blogs");
		if($total_blogs>3 && $activeinactive == "Active")
		{
	?>
		<div class="text-center margin-top-50">
		 <a class="btn btn-primary btn-arrow-right" href="all_blogs.php" target="_blank">More Blog Post</a>
		</div>
	<?php
		}
	?>
</div>

	
<!-----------------------------------------ourblog end------------------------------------------------------------>	

