<!--=============================================Ourteam start=============================================-->
<div class="container containercolor2" id="ourteam">
    <div class="page-header">
       <h2 class="block-title color1">
         Our amazing team</h2>
    </div>
		<section class="section team-section">
			<div class="row text-center">
				<div class="col-lg-3 col-md-6 mb-r">
					<div class="avatar">
						<img src="<?php echo $GLOBAL_CONFIG["crm_image_url"].rtrim($detail_list_arr[0]->name_value_list->first_designation_image_c->value, "^");  ?>" />
					</div>
						<h4><?php echo $detail_list_arr[0]->name_value_list->first_designation_name_c->value  ?></h4>
						<h5><?php echo $detail_list_arr[0]->name_value_list->first_designation_c->value  ?></h5>
							<a class="icons-sm fb-ic"><i class="fa fa-facebook"> </i></a>
							<a class="icons-sm tw-ic"><i class="fa fa-twitter"> </i></a>
							<a class="icons-sm drib-ic"><i class="fa fa-dribbble"> </i></a>
				</div>
       
				<div class="col-lg-3 col-md-6 mb-r">
					<div class="avatar">
						<img src="<?php echo $GLOBAL_CONFIG["crm_image_url"].rtrim($detail_list_arr[0]->name_value_list->second_designation_image_c->value, "^");  ?>" />
					</div>
					<h4><?php echo $detail_list_arr[0]->name_value_list->second_designation_name_c->value  ?></h4>
					<h5><?php echo $detail_list_arr[0]->name_value_list->second_designation_c->value  ?></h5>
					
						<a class="icons-sm li-ic"><i class="fa fa-linkedin"> </i></a>
						<a class="icons-sm fb-ic"><i class="fa fa-facebook"> </i></a>
						<a class="icons-sm git-ic"><i class="fa fa-github"> </i></a>
				</div>
   
				<div class="col-lg-3 col-md-6 mb-r">
					<div class="avatar">
						<img src="<?php echo $GLOBAL_CONFIG["crm_image_url"].rtrim($detail_list_arr[0]->name_value_list->third_designation_image_c->value, "^");  ?>" />
					</div>
					<h4><?php echo $detail_list_arr[0]->name_value_list->third_designation_name_c->value  ?></h4>
					<h5><?php echo $detail_list_arr[0]->name_value_list->third_designation_c->value  ?></h5>
					
					<a class="icons-sm fb-ic"><i class="fa fa-facebook"> </i></a>
					<a class="icons-sm pin-ic"><i class="fa fa-pinterest"> </i></a>
					<a class="icons-sm ins-ic"><i class="fa fa-instagram"> </i></a>
				</div>
     
				<div class="col-lg-3 col-md-6 mb-r">
					<div class="avatar">
						<img src="<?php echo $GLOBAL_CONFIG["crm_image_url"].rtrim($detail_list_arr[0]->name_value_list->fourth_designation_image_c->value, "^");  ?>" />
					</div>
					<h4><?php echo $detail_list_arr[0]->name_value_list->fourth_designation_name_c->value  ?></h4>
					<h5><?php echo $detail_list_arr[0]->name_value_list->fourth_designation_c->value  ?></h5>
					
					<a class="icons-sm gplus-ic"><i class="fa fa-google-plus"> </i></a>
					<a class="icons-sm fb-ic"><i class="fa fa-facebook"> </i></a>
					<a class="icons-sm git-ic"><i class="fa fa-github"> </i></a>
				</div>
			</div>
	</section>
</div>
<!--------------------------------------------------ourteam end------------------------------------------------>
