<!-----------------------------------------About start------------------------------------------------->	

<div class="container containercolor2" id="about">
    <div class="page-header">
        <h2 class="block-title color1">
        About Us</h2>
	<div class="row">
      <div class="col-md-10 col-md-push-1">
		<?php
			$aboutimagepath = $mainfunc->image_paths($detail_list_arr[0]->name_value_list->about_page_banner_c->value, "assets/images/no_image_available.jpg", '1');
			if($aboutimagepath !="")
			{
				foreach($aboutimagepath as $imagepath)
					echo "<img  src='".$imagepath."' class='aboutimg'' alt='About Pic' />";
			}
		?>
      </div>
        <div class="col-md-10 col-md-push-1">
      <!--  <p class="about"><?php
			//$pagecontent = html_entity_decode($detail_list_arr[0]->name_value_list->about_us_c->value);
			//('/<span>(.*?)<\/span>/', $pagecontent, $match);
			//echo $match[1];
		?></p>-->
		<p class="about">
		 <?php
			$aboutextsize = 100;
			$pagecontent =strip_tags(html_entity_decode($detail_list_arr[0]->name_value_list->about_us_c->value));
			if(strlen($pagecontent)>$aboutextsize)
			{
				$pagecontent1= rtrim($mainfunc->get_approx_chars($pagecontent, $aboutextsize), "...");
				$morecontent = substr($pagecontent, strlen($pagecontent1));
				//echo $pagecontent1."<span class='morecontent'>$morecontent</span><a href='javascript:void(0)' class='readmore'>...Read More</a>";
				echo $pagecontent1."<span class='morecontent'>$pagecontent1</span><a href='about.php' class='readmore'>...Read More</a>";
			}
			else
			{
				echo $pagecontent;
			}
		?></p>
      </div>
      
    </div>
  </div>
</div>

