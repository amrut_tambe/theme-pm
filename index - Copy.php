<?php include("includes/header.php");?>
<?php include("includes/slider.php");?>
<!-----------------------------------------About start------------------------------------------------->	

<div class="container containercolor2" id="about">
    <div class="page-header">
        <h2 class="block-title color1">
        About Us</h2>
		 
 
    <div class="row">
      <div class="col-md-7">
	   <?php
				//echo "Check Image path : ".$viewable_page_content_arr["About"]->images_c->value;exit;
				$aboutimagestr = rtrim($detail_list_arr[0]->name_value_list->about_page_banner_c->value, "^");
				$aboutimagearr = explode("^", $aboutimagestr);
			   ?>
				<img src="<?php echo $GLOBAL_CONFIG["crm_image_url"].end($aboutimagearr);?>" width="100%" height="500px;">
        
      </div>
      <div class="col-md-5">
        <p><?php
			$pagecontent = html_entity_decode($detail_list_arr[0]->name_value_list->about_us_c->value);
			preg_match('/<span>(.*?)<\/span>/', $pagecontent, $match);
			echo $match[1];
		?></p>
      </div>
      
    </div>
  </div>
</div>




<!--<div class="container containercolor2" id="about">
    <div class="page-header">
        <h2 class="block-title color1">
         About Us</h2>
         <?php //echo ucwords($field_value->about_us_c->label);?></h2>
		 <p><?php
			//$pagecontent = html_entity_decode($detail_list_arr[0]->name_value_list->about_us_c->value);
			//preg_match('/<span>(.*?)<\/span>/', $pagecontent, $match);
			//echo $match[1];
		?>.</p>
    </div>
			
		<div class="row">
			<div class="col-md-12">
			  <?php
				//echo "Check Image path : ".$viewable_page_content_arr["About"]->images_c->value;exit;
				//$aboutimagestr = rtrim($detail_list_arr[0]->name_value_list->about_page_banner_c->value, "^");
				//$aboutimagearr = explode("^", $aboutimagestr);
			   ?>
				<img src="<?php //echo $GLOBAL_CONFIG["crm_image_url"].end($aboutimagearr);?>" width="100%" height="500px;">
			</div>
	
			</div>
   </div>
 </div>
	  
<!==============================================ABOUT end===================================================-->	
	
<!--=============================================Ourteam start=============================================-->
<div class="container containercolor2" id="ourteam">
    <div class="page-header">
       <h2 class="block-title color1">
         Our amazing team</h2>
    </div>
		<section class="section team-section">
			<div class="row text-center">
				<div class="col-lg-3 col-md-6 mb-r">
					<div class="avatar">
						<img src="<?php echo $GLOBAL_CONFIG["crm_image_url"].rtrim($detail_list_arr[0]->name_value_list->first_designation_image_c->value, "^");  ?>" />
					</div>
						<h4><?php echo $detail_list_arr[0]->name_value_list->first_designation_name_c->value  ?></h4>
						<h5><?php echo $detail_list_arr[0]->name_value_list->first_designation_c->value  ?></h5>
							<a class="icons-sm fb-ic"><i class="fa fa-facebook"> </i></a>
							<a class="icons-sm tw-ic"><i class="fa fa-twitter"> </i></a>
							<a class="icons-sm drib-ic"><i class="fa fa-dribbble"> </i></a>
				</div>
       
				<div class="col-lg-3 col-md-6 mb-r">
					<div class="avatar">
						<img src="<?php echo $GLOBAL_CONFIG["crm_image_url"].rtrim($detail_list_arr[0]->name_value_list->second_designation_image_c->value, "^");  ?>" />
					</div>
					<h4><?php echo $detail_list_arr[0]->name_value_list->second_designation_name_c->value  ?></h4>
					<h5><?php echo $detail_list_arr[0]->name_value_list->second_designation_c->value  ?></h5>
					
						<a class="icons-sm li-ic"><i class="fa fa-linkedin"> </i></a>
						<a class="icons-sm fb-ic"><i class="fa fa-facebook"> </i></a>
						<a class="icons-sm git-ic"><i class="fa fa-github"> </i></a>
				</div>
   
				<div class="col-lg-3 col-md-6 mb-r">
					<div class="avatar">
						<img src="<?php echo $GLOBAL_CONFIG["crm_image_url"].rtrim($detail_list_arr[0]->name_value_list->third_designation_image_c->value, "^");  ?>" />
					</div>
					<h4><?php echo $detail_list_arr[0]->name_value_list->third_designation_name_c->value  ?></h4>
					<h5><?php echo $detail_list_arr[0]->name_value_list->third_designation_c->value  ?></h5>
					
					<a class="icons-sm fb-ic"><i class="fa fa-facebook"> </i></a>
					<a class="icons-sm pin-ic"><i class="fa fa-pinterest"> </i></a>
					<a class="icons-sm ins-ic"><i class="fa fa-instagram"> </i></a>
				</div>
     
				<div class="col-lg-3 col-md-6 mb-r">
					<div class="avatar">
						<img src="<?php echo $GLOBAL_CONFIG["crm_image_url"].rtrim($detail_list_arr[0]->name_value_list->fourth_designation_image_c->value, "^");  ?>" />
					</div>
					<h4><?php echo $detail_list_arr[0]->name_value_list->fourth_designation_name_c->value  ?></h4>
					<h5><?php echo $detail_list_arr[0]->name_value_list->fourth_designation_c->value  ?></h5>
					
					<a class="icons-sm gplus-ic"><i class="fa fa-google-plus"> </i></a>
					<a class="icons-sm fb-ic"><i class="fa fa-facebook"> </i></a>
					<a class="icons-sm git-ic"><i class="fa fa-github"> </i></a>
				</div>
			</div>
	</section>
</div>
<!--------------------------------------------------ourteam end------------------------------------------------>

<!---------------------------------------------------testimonial start-------------------------------------->
<div class="container containercolor2" id="testimonial">
    <div class="page-header">
       <h2 class="block-title color1">
         Testimonial</h2>
    </div>
	   <hr>
    <div class="row">
       <div class="col-md-10 col-md-offset-1">
			<div class="row testimonials">
			 <?php
					$module_id='ed8e3384-d10a-7e8a-ce40-593a5f712064';
					$module_name="Accounts";
					$link_field_name="accounts_abc12_feedbacks_1";
					$related_module_query="status_c='Active'";
					$related_fields= array("created_by_name", "upload_feedback_c", "description");
					$related_module_link_name_to_fields_array=array();
					$deleted=0;
					$order_by="";  
					$limit=200;
					$offset=0;
					$testimonial_list = $sugar->get_relationship_list($session_id,$module_name,$module_id,$link_field_name,$related_module_query,$related_fields,$related_module_link_name_to_fields_array,$deleted,$order_by,$offset,$limit); //fetching Sub panel value
					$testimonial_value_arr=$testimonial_list->entry_list;
					 // echo '<pre>';
					// print_r($testimonial_value_arr);
					// echo '</pre>';
					/*exit; */
					foreach($testimonial_value_arr as $testimonial)
					{
					if(isset($testimonial->name_value_list->upload_feedback_c->value) && trim($testimonial->name_value_list->upload_feedback_c->value)!="")
					$imagepath=$GLOBAL_CONFIG["crm_image_url"].str_replace("^", "", $testimonial->name_value_list->upload_feedback_c->value);
					else
					$imagepath="images/testimonial/2.png";
					//echo $imagepath;
				?>
				<div class="col-sm-4">
				
					<blockquote>
						<p class="clients-words"><?php echo $testimonial->name_value_list->description->value;?></p>
						<span class="clients-name text-primary">— <?php echo $testimonial->name_value_list->created_by_name->value;?></span>
						
						<img src="<?php echo $imagepath;?>" alt=""  class="img-circle img-thumbnail">
					</blockquote>
					
				</div>
				<?php
					}
					?>	
				
			</div><!--/.row-->
			
		</div><!--/.col-->	
	</div><!--/.row-->
</div>
<!------------------------------------testimonial end------------------------------------------------>
<div class="container containercolor2" id="testimonial">
    <div class="page-header">
       <h2 class="block-title color1">
         Testimonial</h2>
    </div>
<section id="carousel">    				
	<div class="container">
		<div class="row">
			<div class="col-md-8 col-md-offset-2">
                <div class="quote"><i class="fa fa-quote-left fa-4x"></i></div>
				<div class="carousel slide" id="fade-quote-carousel" data-ride="carousel" data-interval="3000">
				  <!-- Carousel indicators -->
                  <ol class="carousel-indicators">
				    <li data-target="#fade-quote-carousel" data-slide-to="0" class="active"></li>
				    <li data-target="#fade-quote-carousel" data-slide-to="1"></li>
				    <li data-target="#fade-quote-carousel" data-slide-to="2"></li>
                    <li data-target="#fade-quote-carousel" data-slide-to="3"></li>
                    <li data-target="#fade-quote-carousel" data-slide-to="4"></li>
                    <li data-target="#fade-quote-carousel" data-slide-to="5"></li>
				  </ol>
				  <!-- Carousel items -->
				  <div class="carousel-inner">
				    <?php
					$module_id='ed8e3384-d10a-7e8a-ce40-593a5f712064';
					$module_name="Accounts";
					$link_field_name="accounts_abc12_feedbacks_1";
					$related_module_query="status_c='Active'";
					$related_fields= array("created_by_name", "upload_feedback_c", "description");
					$related_module_link_name_to_fields_array=array();
					$deleted=0;
					$order_by="";  
					$limit=200;
					$offset=0;
					$testimonial_list = $sugar->get_relationship_list($session_id,$module_name,$module_id,$link_field_name,$related_module_query,$related_fields,$related_module_link_name_to_fields_array,$deleted,$order_by,$offset,$limit); //fetching Sub panel value
					$testimonial_value_arr=$testimonial_list->entry_list;
					 // echo '<pre>';
					// print_r($testimonial_value_arr);
					// echo '</pre>';
					/*exit; */
					foreach($testimonial_value_arr as $testimonial)
					{
					if(isset($testimonial->name_value_list->upload_feedback_c->value) && trim($testimonial->name_value_list->upload_feedback_c->value)!="")
					$imagepath=$GLOBAL_CONFIG["crm_image_url"].str_replace("^", "", $testimonial->name_value_list->upload_feedback_c->value);
					else
					$imagepath="images/testimonial/2.png";
					//echo $imagepath;
				?>
				  <div class="item">
				 
				   <img src="<?php echo $imagepath;?>" alt=""  class="profile-circle">
					<blockquote>
						<h3><?php echo $testimonial->name_value_list->created_by_name->value;?></h3>
						<p><?php echo $testimonial->name_value_list->description->value;?></p>
					</blockquote>	
					
				</div>
				<?php
					}
					?>
			</div>
		</div>
	</div>							
</div>
	</div>
		</section>
			</div>
<!-----------------------------------------Our blog start------------------------------------------------>	
	    
	
	<div class="container containercolor2" id="blog">
    <div class="page-header">
       <h2 class="block-title color1">
         Our Blog</h2>
    </div>
    <div class="row">
	<?php
		$module_id='9b651a15-7099-7796-af56-593a4d67e817';
		$module_name="abc12_Website";
		$link_field_name="abc12_website_abc12_blog_1";
		$related_module_query="";
		$related_fields= array("id","name", "blog_page_c", "blog_page_content_c","date_entered","created_by_name" );
		$related_module_link_name_to_fields_array=array();
		$deleted=0;
		$order_by="date_created desc";  
		$limit=50;
		$offset=0;
		$blog_list = $sugar->get_relationship_list($session_id,$module_name,$module_id,$link_field_name,$related_module_query,$related_fields,$related_module_link_name_to_fields_array,$deleted,$order_by,$offset,$limit); //fetching Sub panel value
		$blog_value_arr=$blog_list->entry_list;
		 // echo '<pre>';
		// print_r($blog_value_arr);
		// echo '</pre>';
		// exit;
		$blogarr=array();
		foreach($blog_value_arr as $blog)
		
		$blogarr[]=$blog;
		if(isset($blogarr[0]->name_value_list->blog_page_c->value) && trim($blog->name_value_list->blog_page_c->value)!="")
		$imagepath=$GLOBAL_CONFIG["crm_image_url"].str_replace("^", "", $blog->name_value_list->blog_page_c->value);
		else
		$imagepath="img\showcase/project3.png";
		//echo $imagepath;
	?>
		<div class="col-md-5 col-lg-5">
			<div class="featured-article">
				<a href="#">
					<img src="<?php echo $imagepath[0];?>" alt=""/style="height:100px;width:100px;">
				</a>
				<div class="block-title">
					<h2><?php echo substr($blogarr[0]->name_value_list->name->value,0,25)." ...";?></h2>
					<p class="by-author"><small><?php echo $blogarr[0]->name_value_list->created_by_name->value;?></small></p>
					<p class="by-author"><?php echo $blogarr[0]->name_value_list->date_entered->value;?></p>
						<a href="blog_details.php?id=<?php echo $blog->name_value_list->id->value;?>" target="_blank"class="hs_read_more hs_btn1">read more</a>
				</div>
			</div>
			<!-- /.featured-article -->
		</div>
		<div class="col-md-7 col-lg-7">
		
			<ul class="media-list main-list">
			<?php
				for($i = 1; $i<count($blogarr); $i++)
				{
			?>
			  <li class="media">
			    <a class="pull-left" href="#">
			     
				  <img src="<?php echo $imagepath;?>" alt=""/style="height:100px;width:100px;">
			    </a>
			    <div class="media-body">
			      <h4 class="media-heading"><?php echo substr($blogarr[1]->name_value_list->name->value,0,25)." ...";?></h4>
			      <p class="by-author"><?php echo $blogarr[1]->name_value_list->created_by_name->value;?>
				  <?php echo $blogarr[1]->name_value_list->date_entered->value;?></p>
				  <p>
				  <?php
						$pagecontent = substr(strip_tags(html_entity_decode($blogarr[1]->name_value_list->blog_page_content_c->value)),0,150)."...";
						echo $pagecontent;?>
					</p>
						<a href="blog_details.php?id=<?php echo $blog->name_value_list->id->value;?>" target="_blank"class="hs_read_more hs_btn1">read more</a>
			    </div>
			  </li>
			  <?php
				}
			  ?>
			</ul>
		</div>
	</div>
  
</div>

	
<!-----------------------------------------ourblog end------------------------------------------------------------>	


<!-----------------------------------------feature section------------------------------------------------------------>	
<div class="container containercolor2" id="feature">
    <div class="page-header">
       <h2 class="block-title color1">
         Latest Features</h2>
    </div>
    <div class="row text-center">
		<?php
		$module_id='a5ce212a-4470-b210-0d83-593aaf751f19';
		$module_name="AOS_Products";
		$link_field_name="aos_products_abc12_product_feature_1";
		$related_module_query="";
		$related_fields= array("id", "name");
		$related_module_link_name_to_fields_array=array();
		$deleted=0;
		$order_by="";  
		$limit=200;
		$offset=0;
		$product_list = $sugar->get_relationship_list($session_id,$module_name,$module_id,$link_field_name,$related_module_query,$related_fields,$related_module_link_name_to_fields_array,$deleted,$order_by,$offset,$limit); //fetching Sub panel value
		$product_value_arr=$product_list->entry_list;
		
		//echo '<pre>';
		//print_r($product_value_arr);
		
		$prodid_arr  = array();
		foreach($product_value_arr as $product)
		$prodid_arr[] = "abc12_product_feature.id='".$product->name_value_list->id->value."'";
		//echo '<pre>';
		//print_r($prodid_arr);

		$prodquerycondition = implode(" OR ", $prodid_arr);
		//echo '<pre>';
		//print_r($prodquerycondition);
		$fields= array("name", "description","image_c");
		$query1=$prodquerycondition;
		$link_name_to_fields_array1="";
		$module_name="abc12_Product_Feature"; 
		$order_by=""; 
		$max_results=50;
		$deleted=0;
		$product_detail_list = $sugar->get_entry_list($session_id,$module_name,$query1,$order_by,0,$fields,$link_name_to_fields_array1,$max_results,0,false); 
		$product_detail_list_arr=$product_detail_list->entry_list;
		// echo '<pre>';
		// print_r($product_detail_list_arr);
		// echo '</pre>'; 
		//exit;
		foreach($product_detail_list_arr as $product_detail)
		{
			// echo '<pre>';
			// print_r($product_detail);
			// echo'<pre>';
			
		if(isset($product_detail->name_value_list->image_c->value) && trim($product_detail->name_value_list->image_c->value)!="")
		$imagepath=$GLOBAL_CONFIG["crm_image_url"].str_replace("^", "", $product_detail->name_value_list->image_c->value);
		else
		$imagepath="img\showcase/project3.png";
		//echo $imagepath;
	?>
				
		<div class="col-md-3 col-sm-6 hero-feature">
			<div class="thumbnail">
					<img src="<?php echo $imagepath;?>" alt=""/style="height:100px;width:100px;">
					<div class="caption">
						<h3><?php echo html_entity_decode($product_detail->name_value_list->name->value);?></h3>
						<p>
						<?php echo substr($product_detail->name_value_list->description->value,0,35)." ...";?></p>
						<p>
							<a href="feature_details.php?id=<?php echo $product_detail->name_value_list->id->value;?>" target="_blank" class="btn btn-default">More Info</a>
						</p>
					</div>
				</div>
			
			</div>
			<?php 
			}
			?>
		</div>
      </div>
<!-----------------------------------------------feature end------------------------------------------------------------>	  

<!------------------------------------------- contact us start --------------------------------------------------------->

 
 <div class="container containercolor2" id="contact">
    <div class="page-header">
       <h2 class="block-title color1">
         Contact Us</h2>
    </div>
    <div class="row">
	<?php
		$fields= array();
		$query1="accounts.id='ed8e3384-d10a-7e8a-ce40-593a5f712064'";
		$link_name_to_fields_array1="";
		$module_name="Accounts"; 
		$order_by=""; 
		$max_results=50;
		$deleted=0;
		$contact_detail_list = $sugar->get_entry_list($session_id,$module_name,$query1,$order_by,0,$fields,$link_name_to_fields_array1,$max_results,0,false); 
		$contact_detail_arr=$contact_detail_list->entry_list;
		// echo '<pre>';
		// print_r($contact_detail_arr); 
		// echo '</pre>';
	?>
        <div class="col-md-6">
            <div class="well">
                <h3 style="line-height:20%;"><i class="fa fa-home fa-1x" style="line-height:6%;color:#339966"></i> Address</h3>               
                <p style="margin-top:6%;line-height:35%"><?php echo $contact_detail_arr[0]->name_value_list->billing_address_street->value;?><br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<?php echo $contact_detail_arr[0]->name_value_list->billing_address_city->value;?>&nbsp;<?php echo $contact_detail_arr[0]->name_value_list->billing_address_state->value;?>&nbsp;&nbsp;<?php echo $contact_detail_arr[0]->name_value_list->billing_address_postalcode->value;?>&nbsp;<?php echo $contact_detail_arr[0]->name_value_list->billing_address_country->value;?></p>
                <br/>
                <br/>
                <h3 style="line-height:20%;"><i class="fa fa-envelope fa-1x" style="line-height:6%;color:#339966"></i> E-Mail </h3>
                <p style="margin-top:6%;line-height:35%"><a href="mailto:<?php echo $contact_detail_arr[0]->name_value_list->email1->value;?>"><?php echo $contact_detail_arr[0]->name_value_list->email1->value;?></a></p>
                <br />
                <br />
                <h3 style="line-height:20%;"><i class="fa fa-user fa-1x" style="line-height:6%;color:#339966"></i>Name</h3>
                <p style="margin-top:6%;line-height:35%">Poonam Rai</p>
                <br />
                <br />
                <h3 style="line-height:20%;"><i class="fa fa-yelp fa-1x" style="line-height:6%;color:#339966"></i> Contact No</h3>
                <p style="margin-top:6%;line-height:35%"><a href="tel:<?php echo $contact_detail_arr[0]->name_value_list->phone_office->value;?>"><?php echo $contact_detail_arr[0]->name_value_list->phone_office->value;?></a></p>
            </div>
        </div>
        <div class="col-md-6">
          <div class="row">
			<form name="contactform" id="contactform" method="post" data-parsley-validate="">
				<div class="messages"></div>
					<div class="row">
					<div id="contacterrordiv"></div>
                         <div class="col-md-6">
                            <div class="form-group">
							<?php 
								$data=array();
								$data['type']='text';
								$data['name']='first_name';
								$data['required']='1';
								$data['id']='first_name';
								$modulename='Users';
								$data['placeholder']='Write First Name Here';
								$data['validation_field_name']='First Name'; //name is requird field the message show like this.
								$data['errordivid']='contacterrordiv'; //for error div to display parsley errors
								$data['help']=''; //for help text showing
								$data['class']=''; //class name u want to add
								$value=''; //if want to define any value in the text box
								echo $objectdbtype->getdatatype($data,$value,$modulename);
							?>
                           </div>
                        </div>
						<div class="col-md-6">
							<div class="form-group">
							<?php 
								$data=array();
								$data['type']='text';
								$data['name']='last_name';
								$data['required']='';
								$data['id']='last_name';
								$modulename='Users';
								$data['placeholder']='Write Last Name Here';
								$data['validation_field_name']='Last Name'; //name is requird field the message show like this.
								$data['errordivid']='contacterrordiv'; //for error div to display parsley errors
								$data['help']=''; //for help text showing
								$data['class']=''; //class name u want to add
								$value=''; //if want to define any value in the text box
								echo $objectdbtype->getdatatype($data,$value,$modulename);
							?>
                        </div>
                    </div>
                  </div>
                   <div class="row">
                     <div class="col-md-6">
                        <div class="form-group">
							<?php 
								$data=array();
								$data['type']='text';
								$data['name']='email1';
								$data['required']='1';
								$data['id']='email1';
								$modulename='Users';
								$data['placeholder']='Write Email ID Here';
								$data['validation_field_name']='Email ID'; //name is requird field the message show like this.
								$data['errordivid']='contacterrordiv'; //for error div to display parsley errors
								$data['help']=''; //for help text showing
								$data['class']=''; //class name u want to add
								$value=''; //if want to define any value in the text box
								echo $objectdbtype->getdatatype($data,$value,$modulename);
							?>
                        </div>
                    </div>
                     <div class="col-md-6">
                         <div class="form-group">
						 <?php 
								$data=array();
								$data['type']='phone';
								$data['name']='phone_mobile1';
								$data['required']='1';
								$data['id']='phone_mobile1';
								$modulename='Users';
								$data['placeholder']='Your Mobile number here';
								$data['validation_field_name']='Mobile number'; //name is requird field the message show like this.
								$data['errordivid']='contacterrordiv'; //for error div to display parsley errors
								$data['help']=''; //for help text showing
								$data['class']=''; //class name u want to add
								$value=''; //if want to define any value in the text box
								echo $objectdbtype->getdatatype($data,$value,$modulename);
							?>
                        </div>
                     </div>
                 </div>
                  <div class="row">
                     <div class="col-md-12">
                        <div class="form-group">
						<?php 
								$data=array();
								$data['type']='text';
								$data['name']='description';
								$data['required']='1';
								$data['id']='description';
								$modulename='Users';
								$data['placeholder']='Write your description Here';
								$data['validation_field_name']='Description'; //name is requird field the message show like this.
								$data['errordivid']='contacterrordiv'; //for error div to display parsley errors
								$data['help']=''; //for help text showing
								$data['class']=''; //class name u want to add
								$value=''; //if want to define any value in the text box
								echo $objectdbtype->getdatatype($data,$value,$modulename);
							?>
                        </div>
                     </div>
                     <div class="col-md-12">
						<input type="button" class="btn btn-primary" onclick="save_contact_info();" value="Submit" id="btnsavecontact" />
						
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                         <p class="text-muted"><strong>*</strong> These fields are required. </p>
                     </div>
                 </div>
              </div>
			</form>
		</div> 
	</div>
</div>
	</div>
	
 <div class="container containercolor6" id="contact">
    <h2 class="block-title">Connect with us</h2>
		<div class="row">
			<div class="col-md-12">
			  <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d96690.80542089987!2d29.864461132544537!3d40.77109282810726!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x14cb4f66644bfb9d%3A0x82690ee7586b7eb9!2zxLB6bWl0LCBLb2NhZWxp!5e0!3m2!1str!2str!4v1480782606579" width="100%" height="430" frameborder="0" style="border:0" allowfullscreen></iframe>
		</div>
	</div>
</div>

<?php include("includes/footer.php");