<?php
include_once("./includes/header.php");
?>
    <div class="site-section" style="background-color: #dedffe;">
      <center><h2>Portfolio</h2></center>
      <div class="container">
        <div class="row align-items-stretch">
          <div class="col-md-6 mb-5 mb-lg-5 col-lg-4">
            <div class="service-2 h-100">
              <div class="svg">
                <img src="assets/images/flaticon/svg/001-renovation.svg" alt="Image" class="">
              </div>


              <h3><span>Renovation</span></h3>
              <p>Consectetur adipisicing elit. Numquam repellendus aut labore</p>

            </div>
          </div>
          <div class="col-md-6 mb-5 mb-lg-5 col-lg-4">
            <div class="service-2 h-100">
              <div class="svg">
                <img src="assets/images/flaticon/svg/002-shovel.svg" alt="Image" class="">
              </div>

              <h3><span>Finishing</span></h3>
              <p>Consectetur adipisicing elit. Numquam repellendus aut labore</p>

            </div>
          </div>
          <div class="col-md-6 mb-5 mb-lg-5 col-lg-4">
            <div class="service-2 h-100">
              <div class="svg">
                <img src="assets/images/flaticon/svg/003-bulldozer.svg" alt="Image" class="">
              </div>

              <h3><span>Building Construction</span></h3>
              <p>Consectetur adipisicing elit. Numquam repellendus aut labore</p>

            </div>
          </div>


          <div class="col-md-6 mb-5 mb-lg-5 col-lg-4">
            <div class="service-2 h-100">
              <div class="svg">
                <img src="assets/images/flaticon/svg/004-house-plan.svg" alt="Image" class="">
              </div>

              <h3><span>House Build</span></h3>
              <p>Consectetur adipisicing elit. Numquam repellendus aut labore</p>

            </div>
          </div>
          <div class="col-md-6 mb-5 mb-lg-5 col-lg-4">
            <div class="service-2 h-100">
              <div class="svg">
                <img src="assets/images/flaticon/svg/005-fence.svg" alt="Image" class="">
              </div>

              <h3><span>Fence Construction</span></h3>
              <p>Consectetur adipisicing elit. Numquam repellendus aut labore</p>

            </div>
          </div>
          <div class="col-md-6 mb-5 mb-lg-5 col-lg-4">
            <div class="service-2 h-100">
              <div class="svg">
                <img src="assets/images/flaticon/svg/006-wheelbarrow.svg" alt="Image" class="">
              </div>

              <h3><span>Bridge Construct</span></h3>
              <p>Consectetur adipisicing elit. Numquam repellendus aut labore</p>

            </div>
          </div>

        </div>
      </div>
    </div>
    <section id="about" class="page-section page">
  <div class="container text-center">
    <div class="heading">
      <h1 class="main-title">About Us</h1>  
    </div>
    
    <div class="site-section">
      <div class="container">
        <div class="row">
          <div class="col-lg-6 mb-5 mb-lg-0">
            <img src="assets/images/hero_1.jpg" alt="Image" class="img-fluid">
          </div>
          <div class="col-lg-4 ml-auto">
            <!--<h2>About us</h2>-->
            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Odit suscipit, repudiandae similique accusantium eius nulla quam laudantium sequi.</p>
            <p>Debitis voluptates corporis saepe molestias tenetur ab quae, quo earum commodi, laborum dolore, fuga aliquid delectus cum ipsa?</p>
          </div>
        </div>
      </div>
    </div>

<div class="l-main-container">

  <section id="services"  class="services section-space-padding">
        <div class="container">
            <div class="row">
               <div class="col-md-12 text-center marb-35">
                <div class="container containercolor2" id="service">
    <div class="page-header">
    <center><h1 class="block-title color1 text-center">
         Our Service</h1>
    
    <div class="site-section">
      <div class="container">
        <div class="row mb-4">

          <div class="col-md-4 mx-auto">
            <!--<h2 class="line-bottom text-center">Our Services</h2>-->
          </div>

        </div>

        <div class="row">
          <div class="col-md-6 mb-5 mb-lg-5 col-lg-4">
            <div class="project-item">

              <img src="assets/images/img_1.jpg" alt="Image" class="img-fluid">
              
              <div class="project-item-overlay">
                <a class="category" href="#">Renovate</a>
                <span class="plus">
                  <span class="icon-plus"></span>
                </span>

                <a href="#" class="project-title"><span>Renovate the house</span></a>
              </div>

            </div>
          </div>

          <div class="col-md-6 mb-5 mb-lg-5 col-lg-4">
            <div class="project-item">

              <img src="assets/images/img_2.jpg" alt="Image" class="img-fluid">
              
              <div class="project-item-overlay">
                <a class="category" href="#">Renovate</a>
                <span class="plus">
                  <span class="icon-plus"></span>
                </span>

                <a href="#" class="project-title"><span>Renovate the house 2</span></a>
              </div>

            </div>
          </div>

          <div class="col-md-6 mb-5 mb-lg-5 col-lg-4">
            <div class="project-item">

              <img src="assets/images/img_3.jpg" alt="Image" class="img-fluid">
              
              <div class="project-item-overlay">
                <a class="category" href="#">Renovate</a>
                <span class="plus">
                  <span class="icon-plus"></span>
                </span>

                <a href="#" class="project-title"><span>Renovate the house 2</span></a>
              </div>

            </div>
          </div>

          <div class="col-md-6 mb-5 mb-lg-5 col-lg-4">
            <div class="project-item">

              <img src="assets/images/img_4.jpg" alt="Image" class="img-fluid">
              
              <div class="project-item-overlay">
                <a class="category" href="#">Renovate</a>
                <span class="plus">
                  <span class="icon-plus"></span>
                </span>

                <a href="#" class="project-title"><span>Renovate the house</span></a>
              </div>

            </div>
          </div>

          <div class="col-md-6 mb-5 mb-lg-5 col-lg-4">
            <div class="project-item">

              <img src="assets/images/img_1.jpg" alt="Image" class="img-fluid">
              
              <div class="project-item-overlay">
                <a class="category" href="#">Renovate</a>
                <span class="plus">
                  <span class="icon-plus"></span>
                </span>

                <a href="#" class="project-title"><span>Renovate the house 2</span></a>
              </div>

            </div>
          </div>

          <div class="col-md-6 mb-5 mb-lg-5 col-lg-4">
            <div class="project-item">

              <img src="assets/images/img_2.jpg" alt="Image" class="img-fluid">
              
              <div class="project-item-overlay">
                <a class="category" href="#">Renovate</a>
                <span class="plus">
                  <span class="icon-plus"></span>
                </span>

                <a href="#" class="project-title"><span>Renovate the house 2</span></a>
              </div>

            </div>
          </div>



        </div>
      </div>
    </div>

    <section id="blog" class="page-section page">
  <div class="container text-center">
    <div class="heading">
      <h1 class="main-title">Our Blog</h1>  
    </div>
    <div class="site-section">
      <div class="container">
        <div class="row mb-4">

          <div class="col-md-4 mx-auto">
            <!--<h2 class="line-bottom text-center">Our Blog</h2>-->
          </div>

        </div>

        <div class="row">
          <div class="col-lg-4 col-md-6 mb-4">
            <div class="post-entry-1 h-100">
              <a href="single.php">
                <img src="assets/images/img_4.jpg" alt="Image"
                 class="img-fluid">
              </a>
              <div class="post-entry-1-contents">
                
                <h2><a href="single.php">We Are Leader In The Construction World</a></h2>
                <span class="meta d-inline-block mb-3">July 17, 2019 <span class="mx-2">by</span> <a href="#">Admin</a></span>
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Dolores eos soluta, dolore harum molestias consectetur.</p>
              </div>
            </div>
          </div>
          <div class="col-lg-4 col-md-6 mb-4">
            <div class="post-entry-1 h-100">
              <a href="single.php">
                <img src="assets/images/img_2.jpg" alt="Image"
                 class="img-fluid">
              </a>
              <div class="post-entry-1-contents">
                
                <h2><a href="single.php">We Are Leader In The Construction World</a></h2>
                <span class="meta d-inline-block mb-3">July 17, 2019 <span class="mx-2">by</span> <a href="#">Admin</a></span>
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Dolores eos soluta, dolore harum molestias consectetur.</p>
              </div>
            </div>
          </div>

          <div class="col-lg-4 col-md-6 mb-4">
            <div class="post-entry-1 h-100">
              <a href="single.php">
                <img src="assets/images/img_3.jpg" alt="Image"
                 class="img-fluid">
              </a>
              <div class="post-entry-1-contents">
                
                <h2><a href="single.php">We Are Leader In The Construction World</a></h2>
                <span class="meta d-inline-block mb-3">July 17, 2019 <span class="mx-2">by</span> <a href="#">Admin</a></span>
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Dolores eos soluta, dolore harum molestias consectetur.</p>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>



<div class="ftco-blocks-cover-1">
      <div class="ftco-cover-1 overlay innerpage" style="background-image: url('assets/images/hero_2.jpg')">
        <div class="container">
          <div class="row align-items-center justify-content-center">
            <div class="col-lg-6 text-center">
              <h1>Contact Us</h1>
              <p>Lorem ipsum dolor sit amet consectetur adipisicing elit.</p>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="site-section bg-light" id="contact-section">
      <div class="container">
        <div class="row justify-content-center text-center">
        <div class="col-7 text-center mb-5">
          <h2>Contact Us</h2>
          <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Nemo assumenda, dolorum necessitatibus eius earum voluptates sed!</p>
        </div>
      </div>
        <div class="row">
          <div class="col-lg-8 mb-5" >
            <form action="#" method="post">
              <div class="form-group row">
                <div class="col-md-6 mb-4 mb-lg-0">
                  <input type="text" class="form-control" placeholder="First name">
                </div>
                <div class="col-md-6">
                  <input type="text" class="form-control" placeholder="First name">
                </div>
              </div>

              <div class="form-group row">
                <div class="col-md-12">
                  <input type="text" class="form-control" placeholder="Email address">
                </div>
              </div>

              <div class="form-group row">
                <div class="col-md-12">
                  <textarea name="" id="" class="form-control" placeholder="Write your message." cols="30" rows="10"></textarea>
                </div>
              </div>
              <div class="form-group row">
                <div class="col-md-6 mr-auto">
                  <input type="submit" class="btn btn-block btn-primary text-white py-3 px-5" value="Send Message">
                </div>
              </div>
            </form>
          </div>
          <div class="col-lg-4 ml-auto">
            <div class="bg-white p-3 p-md-5">
              <h3 class="text-black mb-4">Contact Info</h3>
              <ul class="list-unstyled footer-link">
                <li class="d-block mb-3">
                  <span class="d-block text-black">Address:</span>
                  <span>34 Street Name, City Name Here, United States</span></li>
                <li class="d-block mb-3"><span class="d-block text-black">Phone:</span><span>+1 242 4942 290</span></li>
                <li class="d-block mb-3"><span class="d-block text-black">Email:</span><span>info@yourdomain.com</span></li>
              </ul>
            </div>
          </div>
        </div>
      </div>
    </div>
 




    
<?php
include_once("./includes/footer.php");
?> 


  </body>

</html>

