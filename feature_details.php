<?php 
if(isset($_GET["id"]) && strlen($_GET["id"])==36)
	$feature_id=trim($_GET["id"]);
else
{
	header("location:index.php");
	exit;
}
include("includes/header.php");?>

<?php
	
	$fields= array();
	$tablename = strtolower($GLOBAL_MODULE_NAME["features"]);
	$query1="$tablename.id='$feature_id'";
	//$query1="";
	// echo $query1;
	$link_name_to_fields_array1="";
	$module_name=$GLOBAL_MODULE_NAME["features"]; 
	$order_by=""; 
	$max_results=100;
	$deleted=0;
	$basic_detail_list = $sugar->get_entry_list($session_id,$module_name,$query1,$order_by,0,$fields,$link_name_to_fields_array1,$max_results,0,false); 
	$detail_list=$basic_detail_list->entry_list;
	// echo '<pre>';
	// print_r($detail_list);
	// echo '</pre>'; 
	// exit;

if(isset($detail_list[0]->name_value_list->image_c->value) && trim($detail_list[0]->name_value_list->image_c->value)!="")

	$imagepath=$GLOBAL_CONFIG["crm_image_url"].str_replace("^", "", $detail_list[0]->name_value_list->image_c->value);
	else
	$imagepath="images/blog/avatar_1.png";
	

	
?>

<section id="featuredetails" class="blogdetail-section">
	<div class="container">
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
               <h3><?php echo html_entity_decode($detail_list[0]->name_value_list->name->value);?></h3>
            </div>
        </div>
        <div class="row">
            <div class="col-md-9 col-sm-12 col-xs-12">
                <div class="col-md-12 col-sm-12 col-xs-12">
					<?php
					$featureimagepath = $mainfunc->image_paths($detail_list_arr[0]->name_value_list->image_c->value, "assets/images/no_image_available.jpg", '1');
					echo "<img  src='".end($featureimagepath)."' width='100%' height='100%' alt='".$detail_list_arr[0]->name_value_list->name->value."' />";
				?>
                </div>
                <div class="col-md-12 col-sm-12 col-xs-12">
                  <p><?php echo $detail_list[0]->name_value_list->description->value;?></p>
                </div>
            </div>
        </div>
       
</section>

<?php include("includes/footer.php");?>
